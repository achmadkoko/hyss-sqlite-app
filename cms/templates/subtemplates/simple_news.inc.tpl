@!hyss if (isset($edit_news)): !@

    @!hyss if (isset($errors)): !@
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h3><span class="glyphicon glyphicon-warning-sign"></span>
                <strong>@!hyss echo $lang['error_headline']; !@</strong></h3>
            <ul class="errors">
                @!hyss foreach ($errors as $error): !@
                    <li>@!hyss if (isset($lang[$error])) echo $lang[$error]; else echo $error; !@</li>
                @!hyss endforeach; !@
            </ul>
        </div>
    @!hyss endif; !@

    <form class="form-horizontal" action="@!hyss echo BASE_URL . PAGE; !@" method="post">
        <div>
            <input type="hidden" name="mode" value="news"/>
            <input type="hidden" name="edit_news_submit" value="true"/>
            @!hyss if (isset($edit_news['id'])): !@
                <input type="hidden" name="id" value="@!hyss echo $edit_news['id']; !@"/>
            @!hyss endif; !@

            <div class="form-group">
                <label for="title" class="col-md-2 control-label">@!hyss echo $lang['simple_news_edit_title']; !@</label>

                <div class="col-md-10">
                    <input id="title" class="form-control" type="text" name="title"
                           value="@!hyss if (isset($edit_news['title'])) echo $edit_news['title']; !@">
                </div>
            </div>

            <div class="form-group">
                <label for="teaser"
                       class="col-md-2 control-label">@!hyss echo $lang['simple_news_edit_teaser']; !@</label>

                <div class="col-md-10">
                    <textarea id="teaser" class="form-control" name="teaser"
                              rows="5">@!hyss if (isset($edit_news['teaser'])) echo $edit_news['teaser']; !@</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="text" class="col-md-2 control-label">@!hyss echo $lang['simple_news_edit_text']; !@</label>

                <div class="col-md-10">
                    <textarea id="text" class="form-control" name="text"
                              rows="10">@!hyss if (isset($edit_news['text'])) echo $edit_news['text']; !@</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="linkname"
                       class="col-md-2 control-label">@!hyss echo $lang['simple_news_edit_linkname']; !@</label>

                <div class="col-md-10">
                    <input id="linkname" class="form-control" type="text" name="linkname"
                           value="@!hyss if (isset($edit_news['linkname'])) echo $edit_news['linkname']; !@">
                </div>
            </div>

            <div class="form-group">
                <label for="newstime"
                       class="col-md-2 control-label">@!hyss echo $lang['simple_news_edit_time']; !@</label>

                <div class="col-md-10">
                    <input id="newstime" class="form-control" type="text" name="time"
                           value="@!hyss if (isset($edit_news['time'])) echo $edit_news['time']; !@"
                           placeholder="@!hyss echo $lang['simple_news_edit_time_format']; !@">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <button class="btn btn-primary" type="submit">@!hyss echo $lang['submit_button_ok']; !@</button>
                </div>
            </div>

        </div>
    </form>

@!hyss elseif (isset($delete_news)): !@

    <p><strong>@!hyss echo $delete_news['title']; !@</strong></p>

    <form action="@!hyss echo BASE_URL . PAGE; !@" method="post">
        <div>
            <input type="hidden" name="delete" value="@!hyss echo $delete_news['id']; !@"/>

            <p><input class="btn btn-danger btn-lg" type="submit" name="confirmed"
                      value="@!hyss echo $lang['delete_news_confirm_submit']; !@"/></p>
        </div>
    </form>

@!hyss
elseif (isset($news_item)): !@

    @!hyss echo $news_item['text']; !@

    @!hyss if ($authorized_to_edit): !@
        <p>
            <a class="btn btn-primary btn-xs" href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $news_item['id']; !@,edit"
               title="@!hyss echo $lang['edit']; !@"><span class="glyphicon glyphicon-pencil"></span></a>
            <a class="btn btn-danger btn-xs"
               href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $news_item['id']; !@,delete"
               title="@!hyss echo $lang['delete']; !@"
               data-delete-confirm="@!hyss echo rawurlencode($lang['simple_news_delete_confirm']); !@">
                <span class="glyphicon glyphicon-remove"></span></a>
        </p>
    @!hyss endif; !@

@!hyss
elseif (isset($news)): !@

    @!hyss if ($authorized_to_edit): !@
        <p><a class="btn btn-success" href="@!hyss echo BASE_URL . PAGE; !@,add_item"><span
                    class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['simple_news_add_item']; !@</a></p>
    @!hyss endif; !@

    @!hyss foreach ($news as $news_item): !@
        <div class="panel panel-default simple-news">
            <div class="panel-heading">@!hyss echo $lang['simple_news_time'][$news_item['id']]; !@
                @!hyss if ($authorized_to_edit): !@
                    <span class="pull-right">
                    <a class="btn btn-primary btn-xs" href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $news_item['id']; !@,edit"
                       title="@!hyss echo $lang['edit']; !@"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a class="btn btn-danger btn-xs" href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $news_item['id']; !@,delete"
                       title="@!hyss echo $lang['delete']; !@"
                       data-delete-confirm="@!hyss echo rawurlencode($lang['simple_news_delete_confirm']); !@"><span
                            class="glyphicon glyphicon-remove"></span></a>
                    </span>
                @!hyss endif; !@
            </div>
            <div class="panel-body">
                <h2>
                    <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $news_item['id']; !@">@!hyss echo $news_item['title']; !@</a>
                </h2>
                @!hyss if (empty($news_item['teaser'])): !@
                    @!hyss echo $news_item['text']; !@
                @!hyss else: !@
                    <p>@!hyss echo $news_item['teaser']; !@</p>
                    <p><a class="btn btn-primary"
                          href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $news_item['id']; !@">@!hyss echo $news_item['linkname']; !@</a>
                    </p>
                @!hyss endif; !@
            </div>
        </div>
    @!hyss endforeach; !@

    @!hyss if ($pagination): !@
        <ul class="pagination">
            @!hyss if ($pagination['previous']): !@
                <li><a href="@!hyss echo BASE_URL . PAGE;
                if ($pagination['previous'] > 1): !@,,@!hyss echo $pagination['previous']; endif; !@">
                    <span  class="glyphicon glyphicon-chevron-left"></span></a></li>@!hyss endif; !@
            @!hyss foreach ($pagination['items'] as $item): !@
                @!hyss if (empty($item)): !@
                    <li><span>&hellip;</span></li>@!hyss elseif ($item == $pagination['current']): !@
                    <li class="active"><span>@!hyss echo $item; !@</span></li>@!hyss
                else: !@
                    <li><a href="@!hyss echo BASE_URL . PAGE;
                    if ($item > 1): !@,,@!hyss echo $item; endif; !@">@!hyss echo $item; !@</a></li>@!hyss endif; !@
            @!hyss endforeach; !@
            @!hyss if ($pagination['next']): !@
                <li><a href="@!hyss echo BASE_URL . PAGE; !@,,@!hyss echo $pagination['next']; !@">
                    <span class="glyphicon glyphicon-chevron-right"></span></a></li>@!hyss endif; !@
        </ul>
    @!hyss endif; !@

@!hyss
else: !@

    <div class="alert alert-warning">@!hyss echo $lang['no_news']; !@</div>

    @!hyss if ($authorized_to_edit): !@
        <p><a class="btn btn-success" href="@!hyss echo BASE_URL . PAGE; !@,add_item">
                <span class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['simple_news_add_item']; !@</a></p>
    @!hyss endif; !@

@!hyss endif; !@
