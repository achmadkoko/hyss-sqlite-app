@!hyss if (isset($thumbnail)): !@
    @!hyss if ($settings['lightbox_enabled']): !@
        <a @!hyss if (isset($thumbnail['class'])): !@ class="@!hyss echo $thumbnail['class']; !@"@!hyss endif; !@
            href="@!hyss echo BASE_URL . MEDIA_DIR . $thumbnail['photo']; !@" data-lightbox>
            <img  src="@!hyss echo BASE_URL . MEDIA_DIR . $thumbnail['image']; !@"
                title="@!hyss echo $thumbnail['title']; !@" alt="@!hyss echo $thumbnail['title']; !@"
                data-subtitle="@!hyss echo $thumbnail['subtitle']; !@"
                data-description="@!hyss echo $thumbnail['description']; !@" width="@!hyss echo $thumbnail['width']; !@"
                height="@!hyss echo $thumbnail['height']; !@"/></a>
    @!hyss else: !@
        <a@!hyss if (isset($thumbnail['class'])): !@ class="@!hyss echo $thumbnail['class']; !@"@!hyss endif; !@
            href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $thumbnail['id']; !@">
            <img src="@!hyss echo BASE_URL . MEDIA_DIR . $thumbnail['image']; !@"
                title="@!hyss echo $thumbnail['title']; !@" alt="@!hyss echo $thumbnail['title']; !@"
                width="@!hyss echo $thumbnail['width']; !@" height="@!hyss echo $thumbnail['height']; !@"/></a>
    @!hyss endif; !@
@!hyss else: !@
    <span class="missing">[ missing image ]</span>
@!hyss endif; !@
