@!hyss $photos_per_row = 3;
if (isset($photos)): !@

    @!hyss for ($i = 0; $i < $number_of_photos; $i = $i + $photos_per_row): !@
        <div class="row">
            @!hyss for ($n = $i; $n < $i + $photos_per_row; ++$n): !@
                <div class="col-xs-4">
                    @!hyss if (isset($photos[$n])): !@

                        @!hyss if ($settings['lightbox_enabled']): !@
                            <div class="thumbnail-wrapper">
                                <a class="thumbnail"
                                   href="@!hyss echo BASE_URL . MEDIA_DIR . $photos[$n]['photo_normal']; !@"
                                   data-lightbox><img
                                        src="@!hyss echo BASE_URL . MEDIA_DIR . $photos[$n]['photo_thumbnail']; !@"
                                        title="@!hyss echo $photos[$n]['title']; !@"
                                        alt="@!hyss echo $photos[$n]['title']; !@"
                                        data-subtitle="@!hyss echo $photos[$n]['subtitle']; !@"
                                        data-description="@!hyss echo $photos[$n]['description']; !@"
                                        width="@!hyss echo $photos[$n]['width']; !@"
                                        height="@!hyss echo $photos[$n]['height']; !@"/></a>
                            </div>
                        @!hyss else: !@
                            <a class="thumbnail"
                               href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photos[$n]['id']; !@"><img
                                    src="@!hyss echo BASE_URL . MEDIA_DIR . $photos[$n]['photo_thumbnail']; !@"
                                    title="@!hyss echo $photos[$n]['title']; !@"
                                    alt="@!hyss echo $photos[$n]['title']; !@"
                                    width="@!hyss echo $photos[$n]['width']; !@"
                                    height="@!hyss $photos[$n]['height']; !@"/></a>
                        @!hyss endif; !@

                    @!hyss else: !@
                    @!hyss endif; !@

                </div>
            @!hyss endfor; !@

        </div>
    @!hyss endfor; !@



@!hyss else: !@
<p><em>No photo in this gallery</em><p>
    @!hyss endif; !@
