<form action="@!hyss echo BASE_URL . PAGE; !@" method="post">

    <div class="input-group">
        <input class="form-control" type="text" name="q">
        <span class="input-group-btn">
        <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div>
</form>

@!hyss if (isset($results)): !@
    <p style="margin-top:20px;"><strong>@!hyss echo $lang['search_number_of_results']; !@</strong>

    <ul id="search">
        @!hyss foreach ($results as $result): !@
            <li>
                <a href="@!hyss echo BASE_URL . $result['page']; !@">@!hyss echo $result['title']; !@</a>@!hyss if ($result['type'] == 1): !@
                    <span class="smallx">
                    - @!hyss echo $lang['search_photo']; !@</span>@!hyss endif; !@@!hyss if ($result['description']): !@
                    <span class="smallx">- @!hyss echo $result['description']; !@</span>@!hyss endif; !@</li>
        @!hyss endforeach; !@
    </ul>

    @!hyss if ($pagination): !@
        <p class="pagination">@!hyss echo $lang['pagination']; !@ [
            @!hyss if ($pagination['previous']): !@ <a href="@!hyss echo BASE_URL . PAGE; !@,,@!hyss echo $q_encoded;
            if ($pagination['previous'] > 1): !@,@!hyss echo $pagination['previous']; endif; !@">&laquo;</a> @!hyss endif; !@
            @!hyss foreach ($pagination['items'] as $item): !@
                @!hyss if (empty($item)): !@ ..@!hyss elseif ($item == $pagination['current']): !@
                    <span class="current">@!hyss echo $item; !@</span>@!hyss
                else: !@ <a href="@!hyss echo BASE_URL . PAGE; !@,,@!hyss echo $q_encoded;
                if ($item > 1): !@,@!hyss echo $item; endif; !@">@!hyss echo $item; !@</a>@!hyss endif; !@
            @!hyss endforeach; !@
            @!hyss if ($pagination['next']): !@
            <a href="@!hyss echo BASE_URL . PAGE; !@,,@!hyss echo $q_encoded; !@,@!hyss echo $pagination['next']; !@">&raquo;</a>@!hyss endif; !@
            ]</p>
    @!hyss endif; !@

@!hyss endif; !@

@!hyss if (isset($q) && empty($results)): !@
    <p><em>@!hyss echo $lang['search_no_results']; !@</em></p>
@!hyss endif; !@
