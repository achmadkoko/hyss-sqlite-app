<div id="comments" class="panel panel-default">
    <div class="panel-heading">
        @!hyss if ($admin): !@
            @!hyss if (empty($comments_closed)): !@
                <a class="btn btn-danger btn-xs pull-right" href="@!hyss echo BASE_URL . PAGE; !@,openclose">
                    <span class="glyphicon glyphicon-lock"></span> @!hyss echo $lang['comments_close']; !@</a>
            @!hyss else: !@
                <a class="btn btn-success btn-xs pull-right" href="@!hyss echo BASE_URL . PAGE; !@,openclose">
                    <span class="glyphicon glyphicon-lock"></span> @!hyss echo $lang['comments_open']; !@</a>
            @!hyss endif; !@
        @!hyss endif; !@
        <h3 class="panel-title">@!hyss echo $lang['comment_headline']; !@
            <span class="badge">@!hyss echo $total_comments; !@</span></h3>
    </div>

    <div class="panel-body">
        @!hyss if (isset($edit_data)): !@
            <form id="commentform" method="post" action="@!hyss echo $BASE_URL . PAGE; !@#comments">
                <div>
                    <input type="hidden" name="current_page" value="@!hyss echo $edit_data['current_page']; !@"/>
                    <input type="hidden" name="id" value="@!hyss echo $edit_data['id']; !@"/>

                    <p><label for="comment_text"><strong>@!hyss echo $lang['comment_edit_text']; !@</strong></label><br/>
                        <textarea id="comment_text" class="form-control" name="comment_text" cols="63"
                                  rows="10">@!hyss echo $edit_data['comment']; !@</textarea></p>

                    <p class="userdata"><label for="name">@!hyss echo $lang['comment_input_name']; !@</label><br/>
                        <input type="text" id="name" class="form-control" name="name"
                               value="@!hyss echo $edit_data['name']; !@" size="30"/></p>

                    <p class="userdata"><label
                            for="email_hp">@!hyss echo $lang['comment_input_email_hp']; !@</label><br/>
                        <input type="text" id="email_hp" class="form-control" name="email_hp"
                               value="@!hyss echo $edit_data['email_hp']; !@" size="30"/></p>

                    <p><input class="btn btn-primary" name="edit_save" type="submit"
                              value="@!hyss echo $lang['comment_input_submit']; !@"/></p>
                </div>
            </form>

        @!hyss else: !@

            @!hyss if ($comments): !@
                @!hyss foreach ($comments as $comment): !@
                    <div class="comment" id="comment-@!hyss echo $comment['id']; !@">
                        <h4 class="author">@!hyss echo $comment['nr']; !@. @!hyss if (isset($comment['hp'])): !@<a
                                href="@!hyss echo $comment['hp']; !@"><strong>@!hyss echo $comment['name']; !@</strong>
                                </a>@!hyss elseif (isset($comment['email'])): !@<a
                                href="mailto:@!hyss echo $comment['email']; !@">
                                <strong>@!hyss echo $comment['name']; !@</strong></a>@!hyss
                            else: !@<strong>@!hyss echo $comment['name']; !@</strong>@!hyss endif; !@
                            , @!hyss echo $lang['comment_time'][$comment['id']]; !@:@!hyss if ($admin): !@
                                <span class="smallx">(@!hyss echo $comment['ip']; !@)</span>@!hyss endif; !@</h4>

                        <p class="text">@!hyss echo $comment['comment']; !@</p>
                        @!hyss if ($admin): !@
                            <div class="comment-admin">
                                <a class="btn btn-primary btn-xs"
                                   href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $current_page; !@,edit,@!hyss echo $comment['id']; !@#comments"
                                   title="@!hyss echo $lang['comment_edit_link']; !@">
                                    <span class="glyphicon glyphicon-pencil"></span></a>
                                <a class="btn btn-danger btn-xs"
                                   href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $current_page; !@,delete,@!hyss echo $comment['id']; !@"
                                   title="@!hyss echo $lang['comment_delete_link']; !@"
                                   data-delete-confirm="@!hyss echo rawurlencode($lang['comment_delete_confirm']); !@">
                                    <span class="glyphicon glyphicon-remove"></span></a>
                            </div>
                        @!hyss endif; !@
                    </div>
                @!hyss endforeach !@
            @!hyss else: !@
                <p class="no-comments"><em>@!hyss echo $lang['comment_no_comments']; !@</em></p>
            @!hyss endif; !@

            @!hyss if ($pagination): !@
                <p class="comment-info">@!hyss echo $lang['comments_pagination_info']; !@</p>
                <ul class="pagination pagination-sm">
                    @!hyss if ($pagination['previous']): !@
                        <li><a href="@!hyss echo BASE_URL . PAGE;
                        if ($pagination['previous'] > 1): !@,@!hyss echo $pagination['previous']; endif; !@#comments">&laquo;</a>
                        </li>@!hyss endif; !@
                    @!hyss foreach ($pagination['items'] as $item): !@
                        @!hyss if (empty($item)): !@
                            <li><span>…</span></li>@!hyss elseif ($item == $pagination['current']): !@
                            <li class="active"><a href="#">@!hyss echo $item; !@</a></li>@!hyss
                        else: !@
                            <li><a href="@!hyss echo BASE_URL . PAGE;
                            if ($item > 1): !@,@!hyss echo $item; endif; !@#comments">@!hyss echo $item; !@</a>
                            </li>@!hyss endif; !@
                    @!hyss endforeach; !@
                    @!hyss if ($pagination['next']): !@
                        <li><a
                            href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $pagination['next']; !@#comments">&raquo;</a>
                        </li>@!hyss endif; !@
                </ul>
            @!hyss endif; !@

            @!hyss if ($errors): !@
                <div id="errors" class="alert alert-danger">
                    <h3><span class="glyphicon glyphicon-warning-sign"></span> @!hyss echo $lang['error_headline']; !@
                    </h3>
                    <ul>
                        @!hyss foreach ($errors as $error): !@
                            <li>@!hyss if (isset($lang[$error])) echo $lang[$error]; else echo $error; !@</li>
                        @!hyss endforeach; !@
                    </ul>
                    <script type="text/javascript">/* <![CDATA[ */
                        location.hash = 'errors';
                        /* ]]> */</script>
                </div>
            @!hyss endif; !@

            @!hyss if (empty($comments_closed)): !@

                @!hyss if (isset($preview) && empty($errors)): !@
                    <div class="alert alert-warning">
                        <h3 id="preview">@!hyss echo $lang['comment_preview_hl']; !@</h3>

                        <div class="comment-preview">
                            <h4 class="author">@!hyss if (isset($preview['hp'])): !@
                                <a href="@!hyss echo $preview['hp']; !@">
                                    <strong>@!hyss echo $preview['name']; !@</strong>
                                </a>@!hyss elseif (isset($preview['email'])): !@
                                <a href="mailto:@!hyss echo $preview['email']; !@">
                                    <strong>@!hyss echo $preview['name']; !@</strong>
                                </a>@!hyss
                                else: !@<strong>@!hyss echo $preview['name']; !@</strong>@!hyss endif; !@
                                , @!hyss echo $lang['comment_time']['preview']; !@:</h4>

                            <p class="text">@!hyss echo $preview['comment_text']; !@</p>
                        </div>
                    </div>
                    <script type="text/javascript">/* <![CDATA[ */
                        location.hash = 'preview';
                        /* ]]> */</script>
                @!hyss endif; !@

                @!hyss if (empty($preview) && empty($errors)): !@
                    <p>
                        <button type="button" class="btn btn-success" data-toggle="collapse"
                                data-target="#commentformwrapper">
                            <span class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['comments_add_comment']; !@
                        </button>
                    </p>
                @!hyss endif; !@

                <div id="commentformwrapper"
                     class="collapse@!hyss if (isset($preview) || $errors): !@ in@!hyss endif; !@">

                    <form id="commentform" method="post" action="@!hyss echo $BASE_URL . PAGE; !@">
                        <div>
                            @!hyss if ($form_session_data): !@
                                <input type="hidden"
                                        name="@!hyss echo $form_session_data['name']; !@"
                                        value="@!hyss echo $form_session_data['id']; !@" />@!hyss endif; !@

                            <div class="form-group">
                                <label for="comment_text">@!hyss echo $lang['comment_input_text']; !@</label><br/>
                                <textarea id="comment_text" class="form-control" name="comment_text" cols="63"
                                          rows="10">@!hyss echo $form_values['comment_text']; !@</textarea>
                            </div>

                            <div class="form-inline">

                                <div class="form-group">
                                    <label class="sr-only" for="name">@!hyss echo $lang['comment_input_name']; !@</label>
                                    <input class="form-control" type="text" id="name" name="name"
                                           value="@!hyss echo $form_values['name']; !@"
                                           placeholder="@!hyss echo $lang['comment_input_name']; !@"/>
                                </div>

                                <div class="form-group">
                                    <label class="sr-only"
                                           for="email_hp">@!hyss echo $lang['comment_input_email_hp']; !@</label>
                                    <input class="form-control" type="text" id="email_hp" name="email_hp"
                                           value="@!hyss echo $form_values['email_hp']; !@"
                                           placeholder="@!hyss echo $lang['comment_input_email_hp']; !@"/>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" name="save"
                                           value="@!hyss echo $lang['comment_input_submit']; !@"@!hyss if (!$form_session): !@ disabled="disabled"@!hyss endif; !@>
                                    <input type="submit" class="btn btn-primary" name="preview"
                                           value="@!hyss echo $lang['comment_input_preview']; !@"/>
                                </div>

                            </div>

                        </div>
                    </form>

                </div>

            @!hyss endif; !@

        @!hyss endif; !@

        @!hyss if ($pingbacks): !@
            <h3 id="pingbacks">@!hyss echo $lang['pingback_headline']; !@</h3>
            <ol id="pingback-list">
                @!hyss foreach ($pingbacks as $pingback): !@
                    <li>
                        <a href="@!hyss echo $pingback['hp']; !@">@!hyss echo $pingback['name']; !@</a><!-- (@!hyss echo $lang['comment_time'][$pingback['id']]; !@)-->
                        @!hyss if ($admin): !@
                            <br/><span class="small">
                                <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $current_page; !@,edit,@!hyss echo $pingback['id']; !@#comments">
                                    <img src="@!hyss echo BASE_URL; !@templates/images/edit_link.png" width="15"
                                        height="10" alt=""/>@!hyss echo $lang['comment_edit_link']; !@</a> &nbsp;
                                <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $current_page; !@,delete,@!hyss echo $pingback['id']; !@"
                                    onclick="return confirm_link('@!hyss echo rawurlencode($lang['comment_delete_confirm']); !@',this,1)">
                                    <img src="@!hyss echo BASE_URL; !@templates/images/delete_link.png" width="13"
                                        height="9" alt=""/>@!hyss echo $lang['comment_delete_link']; !@</a></span>
                        @!hyss endif; !@
                    </li>
                @!hyss endforeach; !@
            </ol>
        @!hyss endif; !@

        @!hyss if (isset($comments_closed)): !@
            <p><em><span class="glyphicon glyphicon-lock"></span> @!hyss echo $lang['comments_closed']; !@</em></p>
        @!hyss endif; !@

    </div>
</div>
