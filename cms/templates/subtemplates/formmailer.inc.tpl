@!hyss if (isset($mail_sent)): !@

    <div class="alert alert-success"><span
            class="glyphicon glyphicon-ok"></span> @!hyss echo $lang['formmailer_mail_sent']; !@</div>

@!hyss else: !@

    @!hyss if (isset($errors)): !@
        <div class="alert alert-danger">
            <h3><span class="glyphicon glyphicon-warning-sign"></span> @!hyss echo $lang['error_headline']; !@</h3>
            <ul>
                @!hyss foreach ($errors as $error): !@
                    <li>@!hyss if (isset($lang[$error])) echo $lang[$error]; else echo $error; !@</li>
                @!hyss endforeach; !@
            </ul>
        </div>
    @!hyss endif; !@

    <form method="post" action="@!hyss echo BASE_URL . PAGE; !@" accept-charset="@!hyss echo CHARSET; !@">
        <input type="hidden" name="send" value="true">

        <div>

            <div class="form-group">
                <label for="email">@!hyss echo $lang['formmailer_label_email']; !@</label>
                <input id="email" class="form-control" type="email" name="email">
            </div>

            <div class="form-group">
                <label for="subject">@!hyss echo $lang['formmailer_label_subject']; !@</label>
                <input id="subject" class="form-control" type="text" name="subject">
            </div>

            <div class="form-group">
                <label for="message">@!hyss echo $lang['formmailer_label_message']; !@</label>
                <textarea id="message" class="form-control" name="message"
                          rows="12">@!hyss if (isset($message)) echo $message; !@</textarea>
            </div>

            <p>
                <button class="btn btn-primary btn-lg" type="submit"><span
                        class="glyphicon glyphicon-envelope"></span> @!hyss echo $lang['formmailer_button_send']; !@
                </button>
            </p>

        </div>
    </form>

@!hyss endif; !@
