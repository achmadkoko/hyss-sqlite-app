@!hyss if (isset($edit_data)): !@
    <h3 id="comments">@!hyss echo $lang['comment_headline']; !@</h3>
    <form id="commentform" method="post"
          action="@!hyss echo $BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@">
        <div>
            <input type="hidden" name="current_page" value="@!hyss echo $edit_data['current_page']; !@"/>
            <input type="hidden" name="id" value="@!hyss echo $edit_data['id']; !@"/>

            <p><label for="comment_text"><strong>@!hyss echo $lang['comment_edit_text']; !@</strong></label><br/>
                <textarea id="comment_text" name="comment_text" cols="63"
                          rows="10">@!hyss echo $edit_data['comment']; !@</textarea></p>

            <p class="userdata"><label for="name">@!hyss echo $lang['comment_input_name']; !@</label><br/>
                <input type="text" id="name" name="name" value="@!hyss echo $edit_data['name']; !@" size="30"/></p>

            <p class="userdata"><label for="email_hp">@!hyss echo $lang['comment_input_email_hp']; !@</label><br/>
                <input type="text" id="email_hp" name="email_hp" value="@!hyss echo $edit_data['email_hp']; !@"
                       size="30"/></p>

            <p><input name="edit_save" type="submit" value="@!hyss echo $lang['comment_input_submit']; !@"/></p>
        </div>
    </form>
@!hyss else: !@
<div id="commentcontainer"
     style="@!hyss if ($show_comments): !@display:block;@!hyss else: !@display:none;@!hyss endif; !@">
    <h3 id="comments">@!hyss echo $lang['comment_headline']; !@</h3>
    @!hyss if ($comments): !@
        @!hyss foreach ($comments as $comment): !@
            <div class="comments">
                <p class="author">#@!hyss echo $comment['nr']; !@ - @!hyss if (isset($comment['hp'])): !@
                    <a href="@!hyss echo $comment['hp']; !@"><strong>@!hyss echo $comment['name']; !@</strong>
                        </a>@!hyss elseif (isset($comment['email'])): !@
                    <a href="mailto:@!hyss echo $comment['email']; !@"><strong>@!hyss echo $comment['name']; !@</strong>
                        </a>@!hyss
                    else: !@<strong>@!hyss echo $comment['name']; !@</strong>@!hyss endif; !@
                    , @!hyss echo $lang['comment_time'][$comment['id']]; !@:@!hyss if ($admin): !@ <span class="smallx">
                        (@!hyss echo $comment['ip']; !@)</span>@!hyss endif; !@</p>

                <p class="text">@!hyss echo $comment['comment']; !@</p>
                @!hyss if ($admin): !@
                    <p class="admin">
                        <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@,edit,@!hyss echo $comment['id']; !@,@!hyss echo $current_page; !@#comments"><img
                                src="@!hyss echo BASE_URL; !@templates/images/edit_link.png" width="15" height="10"
                                alt=""/>@!hyss echo $lang['comment_edit_link']; !@</a> &nbsp;
                        <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@,delete,@!hyss echo $comment['id']; !@,@!hyss echo $current_page; !@#comments"
                            onclick="return confirm_link('@!hyss echo rawurlencode($lang['comment_delete_confirm']); !@',this,1)">
                            <img src="@!hyss echo BASE_URL; !@templates/images/delete_link.png" width="13" height="9"
                                alt=""/>@!hyss echo $lang['comment_delete_link']; !@</a></p>
                @!hyss endif; !@
            </div>
        @!hyss endforeach !@
    @!hyss else: !@
        <p><i>@!hyss echo $lang['comment_no_comments']; !@</i></p>
    @!hyss endif; !@

    @!hyss if ($pagination): !@
        <p class="pagination">@!hyss echo $lang['comments_pagination_info']; !@ [
            @!hyss if ($pagination['previous']): !@ <a
                href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@,0,1@!hyss if ($pagination['previous'] > 1): !@,@!hyss echo $pagination['previous']; endif; !@#comments">&laquo;</a> @!hyss endif; !@
            @!hyss foreach ($pagination['items'] as $item): !@
                @!hyss if (empty($item)): !@ ..@!hyss elseif ($item == $pagination['current']): !@
                    <span class="current">@!hyss echo $item; !@</span>@!hyss
                else: !@ <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@,0,1@!hyss if ($item > 1): !@,@!hyss echo $item; endif; !@#comments">@!hyss echo $item; !@</a>@!hyss endif; !@
            @!hyss endforeach; !@
            @!hyss if ($pagination['next']): !@
            <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@,0,1,@!hyss echo $pagination['next']; !@#comments">&raquo;</a>@!hyss endif; !@
            ]</p>
    @!hyss endif; !@

    @!hyss if ($errors): !@
        <p class="caution">@!hyss echo $lang['error_headline']; !@</p>
        <ul>
            @!hyss foreach ($errors as $error): !@
                <li>@!hyss if (isset($lang[$error])) echo $lang[$error]; else echo $error; !@</li>
            @!hyss endforeach; !@
        </ul>
        <script type="text/javascript">/* <![CDATA[ */
            location.hash = 'errors';
            /* ]]> */</script>
    @!hyss endif; !@

    @!hyss if (isset($preview) && empty($errors)): !@
        <p id="preview" class="comment-preview-hl">@!hyss echo $lang['comment_preview_hl']; !@</p>
        <div class="comment-preview">
            <p class="author">@!hyss if (isset($preview['hp'])): !@<a href="@!hyss echo $preview['hp']; !@">
                    <strong>@!hyss echo $preview['name']; !@</strong></a>@!hyss elseif (isset($preview['email'])): !@
                <a href="mailto:@!hyss echo $preview['email']; !@"><strong>@!hyss echo $preview['name']; !@</strong>
                    </a>@!hyss
                else: !@<strong>@!hyss echo $preview['name']; !@</strong>@!hyss endif; !@
                , @!hyss echo $lang['comment_time']['preview']; !@:</p>

            <p class="text">@!hyss echo $preview['comment_text']; !@</p>
        </div>
        <script type="text/javascript">/* <![CDATA[ */
            location.hash = 'preview';
            /* ]]> */</script>
    @!hyss endif; !@

    <form id="commentform" method="post"
          action="@!hyss echo $BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@,0,1#comments">
        <div>
            @!hyss if ($form_session_data): !@<input type="hidden" name="@!hyss echo $form_session_data['name']; !@"
                                                    value="@!hyss echo $form_session_data['id']; !@" />@!hyss endif; !@
            <p><label for="comment_text"><strong>@!hyss echo $lang['comment_input_text']; !@</strong></label><br/>
                <textarea id="comment_text" name="comment_text" cols="63"
                          rows="10">@!hyss echo $form_values['comment_text']; !@</textarea></p>

            <p class="userdata"><label for="name">@!hyss echo $lang['comment_input_name']; !@</label><br/>
                <input type="text" id="name" name="name" value="@!hyss echo $form_values['name']; !@" size="30"
                       maxlength="30"/></p>

            <p class="userdata"><label for="email_hp">@!hyss echo $lang['comment_input_email_hp']; !@</label><br/>
                <input type="text" id="email_hp" name="email_hp" value="@!hyss echo $form_values['email_hp']; !@"
                       size="30" maxlength="100"/>
                <small>@!hyss echo $lang['comment_note_email']; !@</small>
            </p>
            <p><input name="save" type="submit"
                      value="@!hyss echo $lang['comment_input_submit']; !@"@!hyss if (!$form_session): !@ disabled="disabled"@!hyss endif; !@ />
                <input name="preview" type="submit" value="@!hyss echo $lang['comment_input_preview']; !@"/></p>
        </div>
    </form>
    <p id="show_commentform" style="display:none;"><a href="#commentform" onclick="show_comment_form(); return false"
                                                      class="icon"><img
                src="@!hyss echo BASE_URL; !@templates/images/comment_add.png" alt="" width="16"
                height="16"/><span>@!hyss echo $lang['comments_add_comment']; !@</span></a></p>
    @!hyss if (empty($preview) && empty($errors)): !@
        <script type="text/javascript">/* <![CDATA[ */
            hide_comment_form();
            /* ]]> */</script>
    @!hyss endif; !@

    @!hyss endif; !@
</div>

<p id="commentlink" style="@!hyss if ($show_comments): !@display:none;@!hyss else: !@display:block;@!hyss endif; !@">
    <a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo IMAGE_IDENTIFIER; !@,@!hyss echo $photo_data['id']; !@,0,1#comments"
        title="@!hyss echo $lang['photo_comment_link_title']; !@" class="commentlink"
        onclick="show_comments(); return false">@!hyss echo $lang['number_of_comments']; !@</a></p>
