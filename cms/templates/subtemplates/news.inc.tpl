@!hyss if ($news): !@
    @!hyss foreach ($news as $item): !@

        <div class="news">

            <p class="time">@!hyss echo $lang['news_time'][$item['id']]; !@</p>

            <h2 class="media-heading"><a
                    href="@!hyss echo BASE_URL . $item['page']; !@">@!hyss echo $item['teaser_headline']; !@</a></h2>

            <div class="media">

                @!hyss if (isset($item['teaser_img'])): !@
                    <a class="pull-left thumbnail" href="@!hyss echo BASE_URL . $item['page']; !@">
                        <img class="media-object" src="@!hyss echo BASE_URL . MEDIA_DIR . $item['teaser_img']; !@"
                            alt="@!hyss echo $item['teaser_headline']; !@"
                            width="@!hyss echo $item['teaser_img_width']; !@"
                            height="@!hyss echo $item['teaser_img_height']; !@"/></a>
                @!hyss endif; !@



                @!hyss if (isset($item['teaser'])): !@
                    <p>@!hyss echo $item['teaser']; !@</p>
                @!hyss endif; !@

                <p><a class="btn btn-primary"
                      href="@!hyss echo BASE_URL . $item['page']; !@">@!hyss echo $item['link_name']; !@</a>
                    @!hyss if (isset($item['comments'])): !@
                        <a class="btn btn-default"
                          href="@!hyss echo BASE_URL . $item['page']; !@#comments"
                          class="comments">@!hyss echo $lang['number_of_comments'][$item['id']]; !@</a>
                    @!hyss endif; !@
                </p>

            </div>
        </div>
    @!hyss endforeach; !@

    @!hyss if ($pagination): !@
        <!--<p>@!hyss echo $lang['pagination']; !@</p>-->
        <ul class="pagination">
            @!hyss if ($pagination['previous']): !@
                <li><a href="@!hyss echo BASE_URL . PAGE;
                if ($pagination['previous'] > 1 || $current_category): !@,@!hyss if ($current_category) echo CATEGORY_IDENTIFIER . $current_category_urlencoded; !@@!hyss if ($pagination['previous'] > 1): !@,@!hyss echo $pagination['previous']; endif; endif; !@">&laquo;</a>
                </li>@!hyss endif; !@@!hyss foreach ($pagination['items'] as $item): !@@!hyss if (empty($item)): !@
                <li>..</li>@!hyss elseif ($item == $pagination['current']): !@
                <li class="active"><a href="#">@!hyss echo $item; !@</a></li>@!hyss
            else: !@
                <li><a href="@!hyss echo BASE_URL . PAGE;
                if ($item > 1 || $current_category): !@,@!hyss if ($current_category) echo CATEGORY_IDENTIFIER . $current_category_urlencoded; !@@!hyss if ($item > 1): !@,@!hyss echo $item; endif; endif; !@">@!hyss echo $item; !@</a>
                </li>@!hyss endif; !@@!hyss endforeach; !@@!hyss if ($pagination['next']): !@
                <li><a href="@!hyss echo BASE_URL . PAGE; !@,@!hyss if ($current_category) echo CATEGORY_IDENTIFIER . $current_category_urlencoded; !@@!hyss if ($pagination['next'] > 1): !@,@!hyss echo $pagination['next']; !@@!hyss endif; !@">&raquo;</a>
                </li>@!hyss endif; !@
        </ul>
    @!hyss endif; !@

@!hyss else: !@

    <div class="alert alert-warning">
        @!hyss echo $lang['no_news']; !@
    </div>

@!hyss endif; !@
