@!hyss if (isset($image)): !@
    <img@!hyss if (isset($image['class'])): !@ class="@!hyss echo $image['class']; !@" @!hyss endif; !@
        src="@!hyss echo BASE_URL . MEDIA_DIR . $image['image']; !@" title="@!hyss echo $image['alt']; !@"
        alt="@!hyss echo $image['alt']; !@" width="@!hyss echo $image['width']; !@"
        height="@!hyss echo $image['height']; !@"/>
@!hyss else: !@
    <span class="missing">[ missing image ]</span>
@!hyss endif; !@
