@!hyss if (isset($notes)): !@

    @!hyss foreach ($notes as $note): !@
        <div class="news">
            <p class="time">@!hyss echo $lang['note_time'][$note['id']]; !@</p>

            <h2>@!hyss if ($note['link']): !@
                    <a href="@!hyss echo $note['link']; !@">@!hyss echo $note['title']; !@</a>@!hyss else: !@@!hyss echo $note['title']; !@@!hyss endif; !@
            </h2>
            @!hyss if ($note['text']): !@<p>@!hyss echo $note['text']; !@</p>@!hyss endif; !@
            @!hyss if ($note['linkname']): !@<p class="link">
                <a class="btn btn-primary"
                    href="@!hyss echo $note['link']; !@">@!hyss echo $note['linkname']; !@</a>
                </p>@!hyss endif; !@
        </div>
    @!hyss endforeach; !@

    @!hyss if ($pagination): !@
        <p class="pagination">@!hyss echo $lang['pagination']; !@ [
            @!hyss if ($pagination['previous']): !@ <a href="@!hyss echo BASE_URL . PAGE;
            if ($pagination['previous'] > 1): !@,@!hyss echo $pagination['previous']; endif; !@">&laquo;</a> @!hyss endif; !@
            @!hyss foreach ($pagination['items'] as $item): !@
                @!hyss if (empty($item)): !@ ..@!hyss elseif ($item == $pagination['current']): !@
                    <span class="current">@!hyss echo $item; !@</span>@!hyss
                else: !@ <a href="@!hyss echo BASE_URL . PAGE;
                if ($item > 1): !@,@!hyss echo $item; endif; !@">@!hyss echo $item; !@</a>@!hyss endif; !@
            @!hyss endforeach; !@
            @!hyss if ($pagination['next']): !@ <a
                href="@!hyss echo BASE_URL . PAGE; !@,@!hyss echo $pagination['next']; !@">&raquo;</a>@!hyss endif; !@
            ]</p>
    @!hyss endif; !@

@!hyss else: !@

    @!hyss echo $lang['no_notes']; !@

@!hyss endif; !@
S