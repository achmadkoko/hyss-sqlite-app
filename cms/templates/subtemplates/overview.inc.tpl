@!hyss if (isset($included_pages)): !@

    @!hyss for ($i = 0; $i < $included_pages_number; $i = $i + 2): !@
        <div class="row">
            @!hyss for ($n = $i; $n < $i + 2; ++$n): !@
                <div class="col-md-6">

                    @!hyss if (isset($included_pages[$n])): !@
                    <div class="overview">
                        <h2 class="teaser"><a
                                href="@!hyss echo BASE_URL . $included_pages[$n]['page']; !@">@!hyss echo $included_pages[$n]['teaser_headline']; !@</a>
                        </h2>

                        <div class="media">
                            @!hyss if ($included_pages[$n]['teaser_img']): !@
                                <a class="thumbnail thumbnail-left"
                                   href="@!hyss echo BASE_URL . $included_pages[$n]['page']; !@"><img
                                        src="@!hyss echo BASE_URL . MEDIA_DIR . $included_pages[$n]['teaser_img']; !@"
                                        alt="@!hyss echo $included_pages[$n]['teaser_headline']; !@"
                                        width="@!hyss echo $included_pages[$n]['teaser_img_width']; !@"
                                        height="@!hyss echo $included_pages[$n]['teaser_img_height']; !@"/></a>
                            @!hyss endif; !@
                            <p class="teaser">@!hyss echo $included_pages[$n]['teaser']; !@</p>

                            <p><a class="btn btn-primary"
                                  href="@!hyss echo BASE_URL . $included_pages[$n]['page']; !@">@!hyss echo $included_pages[$n]['link_name']; !@</a>
                            </p>
                            @!hyss endif; !@
                        </div>
                    </div>
                </div>
            @!hyss endfor; !@

        </div>
    @!hyss endfor; !@

@!hyss endif; !@
