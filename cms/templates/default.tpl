<!DOCTYPE html>
<html lang="@!hyss echo $lang['lang']; !@" dir="@!hyss echo $lang['dir']; !@">
<head>
    <meta charset="@!hyss echo $lang['charset']; !@"/>
    <title>@!hyss if ($page_title): !@@!hyss echo $page_title; !@@!hyss else: !@@!hyss if ($title): !@@!hyss echo $title; !@ - @!hyss endif; !@@!hyss echo $settings['website_title']; !@@!hyss endif; !@</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@!hyss if ($description) echo $description; else echo $settings['default_description']; !@">
    <meta name="keywords" content="@!hyss if ($keywords) echo $keywords; else echo $settings['default_keywords']; !@">
    <meta name="generator" content="hyssSQLiteApp @!hyss echo $settings['version']; !@">
    <link href="@!hyss echo BOOTSTRAP_CSS; !@" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href="@!hyss echo STATIC_URL; !@css/style.css" rel="stylesheet">
    <link rel="shortcut icon" href="@!hyss echo STATIC_URL; !@img/favicon.ico">
</head>

<body@!hyss if ($admin): !@ class="admin"@!hyss endif; !@>
@!hyss if ($admin) include(BASE_PATH . 'cms/templates/admin/subtemplates/admin_menu.inc.tpl'); !@

<div class="container">

    <header class="header">
        <div class="row">
            <div class="col-md-4 logo-wrapper">
                <h1 id="logo"><a href="@!hyss echo BASE_URL; !@">@!hyss echo $settings['website_title']; !@</a></h1>
            </div>
            <a id="menu-toggle-handle" href="#" class="visible-xs"><span class="icon-bar"></span>
                <span class="icon-bar"></span><span class="icon-bar"></span></a>
            <nav id="nav" class="col-md-8">
                @!hyss if ($menu_1 && isset($menus[$menu_1])): !@
                    <ul class="nav nav-pills">
                        @!hyss foreach ($menus[$menu_1] as $item): !@
                            <li@!hyss if (!empty($item['section']) && $item['section'] == $section[0]): !@ class="active"@!hyss endif; !@>
                            <a href="@!hyss echo $item['link']; !@"
                               title="@!hyss echo $item['title']; !@"@!hyss if ($item['accesskey'] != ''): !@ accesskey="@!hyss echo $item['accesskey']; !@"@!hyss endif; !@>@!hyss echo $item['name']; !@</a>
                            </li>@!hyss endforeach; !@
                    </ul>
                @!hyss endif; !@
            </nav>
        </div>
    </header>

    @!hyss if ($sidebar_1): !@
        @!hyss echo $sidebar_1; !@
    @!hyss endif; !@

    @!hyss if ($breadcrumbs): !@
        <ul class="breadcrumb">
            @!hyss foreach ($breadcrumbs as $breadcrumb): !@
                <li><a href="@!hyss echo BASE_URL . $breadcrumb['page']; !@">@!hyss echo $breadcrumb['title']; !@</a></li>
            @!hyss endforeach; !@
            <li class="active">@!hyss echo $title; !@</li>
        </ul>
    @!hyss endif; !@

    @!hyss if (empty($sidebar_1) && empty($breadcrumbs)): !@
        <hr class="topsep hidden-xs">
    @!hyss endif; !@

    <div class="body-content">

        <div class="row@!hyss if (isset($tv['nocolumns'])): !@ main-content@!hyss endif; !@">

            @!hyss if (empty($tv['nocolumns'])): !@
            <div class="col-md-9 main-content">
                @!hyss endif; !@

                @!hyss if (empty($hide_content)) echo $content; !@
                @!hyss if (isset($subtemplate)) include(BASE_PATH . 'cms/templates/subtemplates/' . $subtemplate); !@

            </div>

            @!hyss if ($sidebar_2): !@
                <div class="col-md-3 sidebar">
                    @!hyss echo $sidebar_2; !@
                </div>
            @!hyss endif; !@

            @!hyss if (empty($tv['nocolumns'])): !@
        </div>
        @!hyss endif; !@

    </div>

    @!hyss if ($sidebar_3): !@
        @!hyss echo $sidebar_3; !@
    @!hyss endif; !@

    <hr class="closure">

    <footer class="row footer">
        <div class="col-lg-12">

            @!hyss if ($gcb_1 && isset($gcb[$gcb_1])): !@
                @!hyss echo $gcb[$gcb_1]; !@
            @!hyss else: !@
                <p>&copy; @!hyss echo date("Y"); !@ @!hyss echo $settings['author']; !@@!hyss if ($type != 'news' && $type != 'search' && $type != 'notes'): !@
                        <br/>@!hyss echo $lang['page_last_modified']; !@@!hyss endif; !@<br/>Powered by
                    <a href="http://bitbucket.org/achmadkoko/hyss-sqlite-app">hyssSQLiteApp</a></p>
            @!hyss endif; !@
        </div>
    </footer>

</div>

<script src="@!hyss echo JQUERY; !@"></script>
<script src="@!hyss echo BOOTSTRAP; !@"></script>
<script src="@!hyss echo STATIC_URL; !@js/main.js"></script>
@!hyss if ($admin): !@
    <script src="@!hyss echo STATIC_URL; !@js/admin_frontend.js"></script>
@!hyss endif; !@
@!hyss if (isset($contains_thumbnails)): !@
    <script src="@!hyss echo STATIC_URL; !@js/mylightbox.js" type="text/javascript"></script>
@!hyss endif; !@
</body>
</html>

