@!hyss if (isset($menu_data)): !@

    <h1><a href="index.hyss">@!hyss echo $lang['administration']; !@</a> &raquo; <a
            href="index.hyss?mode=menus">@!hyss echo $lang['menus']; !@</a> &raquo; <a
            href="index.hyss?mode=menus&amp;edit=@!hyss echo $menu_data['menu']; !@">@!hyss echo str_replace('[menu]', $menu_data['menu'], $lang['edit_menu_hl']); !@</a> &raquo; @!hyss echo $lang['edit_menu_item']; !@
    </h1>

    <form action="index.hyss" method="post">
        <div>
            <input type="hidden" name="mode" value="menus"/>
            <input type="hidden" name="menu" value="@!hyss echo $menu_data['menu']; !@"/>
            <input type="hidden" name="id" value="@!hyss echo $menu_data['id']; !@"/>
            <table class="admin-table" cellspacing="1" cellpadding="5" border="0">
                <tr>
                    <th>@!hyss echo $lang['menu_item_name']; !@</th>
                    <th>@!hyss echo $lang['menu_item_title']; !@</th>
                    <th>@!hyss echo $lang['menu_item_link']; !@</th>
                    <th>@!hyss echo $lang['menu_item_section']; !@</th>
                    <th>@!hyss echo $lang['menu_item_accesskey']; !@</th>
                    <th>&nbsp;</th>
                </tr>
                <tr>
                    <td class="a"><input type="text" name="name" value="@!hyss echo $menu_data['name']; !@" size="10"
                                         style="width:100%;"/></td>
                    <td><input type="text" name="title" value="@!hyss echo $menu_data['title']; !@" size="10"
                               style="width:100%;"/></td>
                    <td><input type="text" name="link" value="@!hyss echo $menu_data['link']; !@" size="10"
                               style="width:100%;"/></td>
                    <td><input type="text" name="section" value="@!hyss echo $menu_data['section']; !@" size="10"
                               style="width:100%;"/></td>
                    <td><input type="text" name="accesskey" value="@!hyss echo $menu_data['accesskey']; !@" size="3"
                               style="width:100%;"/></td>
                    <td><input type="submit" name="edit_menu_item_submitted"
                               value="@!hyss echo $lang['submit_button_ok']; !@"/></td>
                </tr>
            </table>
        </div>
    </form>

@!hyss else: !@

    <p>@!hyss echo $lang['menu_item_doesnt_exist']; !@</p>

@!hyss endif; !@
