<ol class="breadcrumb">
    <li><a href="index.hyss?mode=settings">@!hyss echo $lang['settings']; !@</a></li>
    <li class="active">@!hyss echo $lang['advanced_settings']; !@</li>
</ol>

<h1>@!hyss echo $lang['advanced_settings']; !@</h1>

@!hyss include(BASE_PATH.'cms/templates/admin/subtemplates/errors.inc.tpl'); !@

<form class="form-horizontal" action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="settings"/>
        <input type="hidden" name="settings_submitted" value="true"/>
        @!hyss while (list($key, $val) = each($settings_sorted)): !@

            <div class="form-group">
                <label for="@!hyss echo $key; !@" class="col-md-2 control-label">@!hyss echo $key; !@</label>

                <div class="col-md-5">
                    <input type="text" class="form-control" id="@!hyss echo $key; !@" name="@!hyss echo $key; !@"
                           value="@!hyss echo $val; !@">
                </div>

                <div class="col-md-1">
                    <a class="btn btn-danger btn-xs" href="index.hyss?mode=settings&amp;delete=@!hyss echo $key; !@"
                       title="@!hyss echo $lang['delete']; !@"
                       data-delete-confirm="@!hyss echo rawurlencode($lang['delete_setting_confirm']); !@"><span
                            class="glyphicon glyphicon-remove"></span></a>
                </div>

            </div>

        @!hyss endwhile; !@

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-5">
                <button type="submit" class="btn btn-primary">@!hyss echo $lang['submit_button_ok']; !@</button>
            </div>
        </div>

    </div>
</form>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">@!hyss echo $lang['add_new_setting_var']; !@</h3>
    </div>
    <div class="panel-body">
        <form class="form-inline" action="index.hyss" method="post">
            <div class="form-group">
                <input type="hidden" name="mode" value="settings">
                <input type="hidden" name="new_var_submitted" value="true">
                <label class="sr-only" for="name">@!hyss echo $lang['settings_name']; !@</label>
                <input type="text" class="form-control" id="name" name="name"
                       placeholder="@!hyss echo $lang['settings_name']; !@">
            </div>
            <div class="form-group">
                <label class="sr-only" for="value">@!hyss echo $lang['settings_value']; !@</label>
                <input type="text" class="form-control" id="value" name="value"
                       placeholder="@!hyss echo $lang['settings_value']; !@">
            </div>
            <button type="submit" class="btn btn-default">@!hyss echo $lang['submit_button_ok']; !@</button>
        </form>
    </div>
</div>
