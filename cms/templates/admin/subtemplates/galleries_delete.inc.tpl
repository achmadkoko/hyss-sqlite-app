<ol class="breadcrumb">
    <li><a href="index.hyss?mode=galleries">@!hyss echo $lang['photo_galleries']; !@</a></li>
    <li class="active">@!hyss echo $lang['delete_gallery']; !@</li>
</ol>

<h1>@!hyss echo $lang['delete_gallery']; !@</h1>

<p>@!hyss echo str_replace('[gallery]', $gallery, $lang['delete_gallery_confirm']); !@</p>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="galleries"/>
        <input type="hidden" name="delete_gallery" value="@!hyss echo $gallery; !@"/>
        <input class="btn btn-danger btn-lg" type="submit" name="confirmed"
               value="@!hyss echo $lang['delete_gallery_submit']; !@"/>
    </div>
</form>

