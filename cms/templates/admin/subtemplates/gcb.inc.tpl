<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo $lang['gcb']; !@</h1>
    </div>
    <div class="col-md-2">
        <a href="index.hyss?mode=gcb&amp;add_gcb=true" class="btn btn-success btn-top pull-right"><span
                class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['add_gcb']; !@</a>
    </div>
</div>

@!hyss if (isset($gcbs)): !@

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>@!hyss echo $lang['gcb_identifier']; !@</th>
                <th>@!hyss echo $lang['gcb_content']; !@</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @!hyss $i = 0;
            foreach ($gcbs as $gcb): !@
                <tr>
                    <td>@!hyss echo $gcb['identifier']; !@</td>
                    <td>@!hyss echo $gcb['content']; !@</td>
                    <td class="options nowrap"><a href="index.hyss?mode=gcb&amp;edit=@!hyss echo $gcb['id']; !@"
                                                  title="@!hyss echo $lang['edit']; !@"
                                                  class="btn btn-primary btn-xs"><span
                                class="glyphicon glyphicon-pencil"></span></a>&nbsp; <a
                            href="index.hyss?mode=gcb&amp;delete=@!hyss echo $gcb['id']; !@"
                            title="@!hyss echo $lang['delete']; !@"
                            data-delete-confirm="@!hyss echo rawurlencode($lang['delete_gcb_confirm']); !@"
                            class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a></td>
                </tr>
                @!hyss $i++; endforeach; !@
            </tbody>
        </table>
    </div>

@!hyss else: !@

    <div class="alert alert-warning">
        @!hyss echo $lang['no_gcb']; !@
    </div>

@!hyss endif; !@

