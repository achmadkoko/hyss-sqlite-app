<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">@!hyss echo $lang['insert_image']; !@</h4>
</div>
<div class="modal-body">

    @!hyss if (isset($images)): !@
        <div class="form-group">
            <select id="imageselect" class="form-control" size="20">
                @!hyss foreach ($images as $image): !@
                    <option value="@!hyss echo $image; !@">@!hyss echo $image; !@</option>
                @!hyss endforeach; !@
            </select>
        </div>
        <div class="form-group">
            <label for="image_class">@!hyss echo $lang['insert_image_class']; !@</label>
            <input id="image_class" class="form-control" type="text" name="image_class"
                   value="@!hyss echo $settings['default_image_class']; !@">
        </div>
    @!hyss else: !@
        <div class="alert alert-warning">
            <p>@!hyss echo $lang['no_images']; !@</p>
        </div>
    @!hyss endif; !@

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">@!hyss echo $lang['cancel']; !@</button>
    @!hyss if (isset($images)): !@
        <button id="insert-image" type="button"
                class="btn btn-primary">@!hyss echo $lang['insert_image_button']; !@</button>
    @!hyss endif; !@
</div>
<script>
    $(function () {
        $("#imageselect option").dblclick(function (e) {
            $("#insert-image").click();
        });
        $("#insert-image").click(function (e) {
            if (gallery = $("#imageselect option:selected").val()) {
                if (image_class = $("#image_class").val()) imageCode = "[image:" + gallery + "|" + image_class + "]";
                else imageCode = "[image:" + gallery + "]";
                $($insertField).insertAtCaret(imageCode);
            }
            $('#modal_image').modal('hide');
        });
    });
</script>
