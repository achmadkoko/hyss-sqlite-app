<h1>@!hyss echo $lang['delete_page_headline']; !@</h1>

<div class="alert alert-danger">
    <p><span class="glyphicon glyphicon-warning-sign"></span> <strong>@!hyss echo $lang['caution']; !@</strong></p>
</div>
<p>@!hyss echo str_replace('[page]', stripslashes($page['page']), $lang['delete_page_confirm']); !@</p>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="pages"/>
        <input type="hidden" name="delete_page" value="@!hyss echo $page['id']; !@"/>
        <input type="hidden" name="confirmed" value="true"/>
        <button class="btn btn-danger btn-lg">@!hyss echo $lang['delete_page_submit']; !@</button>
    </div>
</form>

