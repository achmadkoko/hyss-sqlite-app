<h1>@!hyss echo $lang['login']; !@</h1>

@!hyss if (isset($_GET['msg'])): !@
    <div class="alert alert-danger">
        <strong>@!hyss echo $lang['login_failed']; !@</strong>
    </div>
@!hyss endif; !@

<form action="index.hyss" method="post">
    <fieldset>
        <div class="form-group login-form">
            <label for="login">@!hyss echo $lang['login_username']; !@</label>
            <input id="login" type="text" name="username" class="form-control" autofocus/></p>
        </div>

        <div class="form-group login-form">
            <label for="pw">@!hyss echo $lang['login_password']; !@</label>
            <input id="pw" type="password" name="userpw" class="form-control"/>
        </div>

        <input type="submit" class="btn btn-lg btn-primary" value="@!hyss echo $lang['login_submit']; !@"/>

    </fieldset>
</form>



