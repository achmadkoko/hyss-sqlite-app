<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo $lang['notes']; !@</h1>
    </div>
    <div class="col-md-2">
        <a class="btn btn-success btn-top pull-right" href="index.hyss?mode=notes&amp;action=new"><span
                class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['create_note_section']; !@</a>
    </div>
</div>

@!hyss if (isset($note_sections)): !@

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>@!hyss echo $lang['note_section']; !@</th>
                <th colspan="2">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @!hyss $i = 0;
            foreach ($note_sections as $note_section): !@
                <tr>
                    <td>@!hyss echo $note_section; !@</td>
                    <td class="nobreak options">
                        <a class="btn btn-primary btn-xs"
                           href="index.hyss?mode=notes&amp;edit=@!hyss echo $note_section; !@"
                           title="@!hyss echo $lang['edit']; !@">
                           <span class="glyphicon glyphicon-pencil"></span></a>
                        <a class="btn btn-danger btn-xs"
                           href="index.hyss?mode=notes&amp;delete=@!hyss echo $note_section; !@"
                           title="@!hyss echo $lang['delete']; !@"
                           data-delete-confirm="@!hyss echo rawurlencode($lang['delete_this_note_section']); !@">
                           <span class="glyphicon glyphicon-remove"></span></a></td>
                </tr>
                @!hyss $i++; endforeach; !@
            <tbody>
        </table>
    </div>

@!hyss else: !@

    <div class="alert alert-warning">
        @!hyss echo $lang['no_note_sections']; !@</em></p>
    </div>

@!hyss endif; !@
