<ol class="breadcrumb">
    <li><a href="index.hyss?mode=comments">@!hyss echo $lang['comments']; !@</a></li>
    <li class="active">@!hyss echo $lang['edit_comment']; !@</li>
</ol>


<h1>@!hyss echo $lang['edit_comment']; !@</h1>

<form method="post" action="index.hyss">
    <div>
        <input type="hidden" name="mode" value="comments"/>
        <input type="hidden" name="type" value="@!hyss echo $type; !@"/>
        <input type="hidden" name="id" value="@!hyss echo $comment['id']; !@"/>
        <input type="hidden" name="comment_id" value="@!hyss echo $comment_id; !@"/>
        <input type="hidden" name="page" value="@!hyss echo $page; !@"/>
        <input type="hidden" name="edit_submit" value="true"/>

        <div class="form-group">
            <textarea class="form-control" name="comment" cols="60"
                      rows="10">@!hyss echo $comment['comment']; !@</textarea>
        </div>

        <div class="form-group">
            <label for="name">@!hyss echo $lang['comments_name_m']; !@</label>
            <input id="name" class="form-control" type="text" name="name" value="@!hyss echo $comment['name']; !@"/>
        </div>

        <div class="form-group">
            <label for="email_hp">@!hyss echo $lang['comments_email_hp_m']; !@</label>
            <input id="email_hp" class="form-control" type="text" name="email_hp"
                   value="@!hyss echo $comment['email_hp']; !@"/>
        </div>

        <button type="submit" class="btn btn-primary">@!hyss echo $lang['submit_button_ok']; !@</button>

    </div>
</form>

