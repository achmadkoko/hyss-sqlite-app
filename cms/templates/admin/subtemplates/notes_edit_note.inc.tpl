<ol class="breadcrumb">
    <li><a href="index.hyss?mode=notes">@!hyss echo $lang['notes']; !@</a></li>
    <li><a href="index.hyss?mode=notes&amp;edit=@!hyss echo $note['note_section']; !@">@!hyss echo $lang['notes']; !@
            : @!hyss echo $note['note_section']; !@</a></li>
    <li class="active">
        @!hyss if (isset($note['id'])): !@
            @!hyss echo $lang['edit_note']; !@
        @!hyss else: !@
            @!hyss echo $lang['add_note']; !@
        @!hyss endif; !@</li>
</ol>

<h1>
    @!hyss if (isset($note['id'])): !@
        @!hyss echo $lang['edit_note']; !@
    @!hyss else: !@
        @!hyss echo $lang['add_note']; !@
    @!hyss endif; !@
</h1>

@!hyss include(BASE_PATH.'cms/templates/admin/subtemplates/errors.inc.tpl'); !@

<form action="index.hyss" method="post" name="notesform">
    <div>
        <input type="hidden" name="mode" value="notes">
        <input type="hidden" name="text_formatting" value="0">
        <input type="hidden" name="edit_note_submit" value="true">
        @!hyss if (isset($note['id'])): !@
            <input type="hidden" name="id" value="@!hyss echo $note['id']; !@"/>
        @!hyss endif; !@
        <input type="hidden" name="note_section" value="@!hyss echo $note['note_section']; !@"/>

        <div class="form-group">
            <label for="title">@!hyss echo $lang['edit_note_title']; !@</label>
            <input id="title" class="form-control" type="text" name="title"
                   value="@!hyss if (isset($note['title'])) echo $note['title']; !@">
        </div>

        <div class="form-group">
            <label for="text">@!hyss echo $lang['edit_note_text']; !@</label>
            <textarea id="text" class="form-control" name="text"
                      rows="10">@!hyss if (isset($note['text'])) echo $note['text']; !@</textarea>
        </div>

        <div class="form-group">
            <label for="link">@!hyss echo $lang['edit_note_link']; !@</label>
            <input id="link" class="form-control" type="text" name="link"
                   value="@!hyss if (isset($note['link'])) echo $note['link']; !@">
        </div>

        <div class="form-group">
            <label for="linkname">@!hyss echo $lang['edit_note_linkname']; !@</label>
            <input id="linkname" class="form-control" type="text" name="linkname"
                   value="@!hyss if (isset($note['linkname'])) echo $note['linkname']; !@">
        </div>

        <div class="form-group">
            <label for="time">@!hyss echo $lang['edit_note_date_marking']; !@</label>
            <input id="time" class="form-control" type="text" name="time"
                   value="@!hyss if (isset($note['time'])) echo $note['time']; !@"
                   placeholder="@!hyss echo $lang['edit_time_format']; !@">
        </div>

        <button class="btn btn-primary" type="submit">@!hyss echo $lang['submit_button_ok']; !@</button>

    </div>
</form>
