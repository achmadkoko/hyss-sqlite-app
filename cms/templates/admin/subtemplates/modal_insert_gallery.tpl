<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">@!hyss echo $lang['insert_gallery_label']; !@</h4>
</div>
<div class="modal-body">

    @!hyss if (isset($galleries)): !@
        <div class="form-group">
            <select id="galleryselect" class="form-control" name="gallery" size="20">
        </div>
        @!hyss foreach ($galleries as $gallery): !@
            <option
                value="@!hyss echo $gallery; !@"@!hyss if (isset($selected_gallery) && $selected_gallery == $gallery): !@ selected="selected"@!hyss endif; !@>@!hyss echo $gallery; !@</option>
        @!hyss endforeach; !@
        </select>
    @!hyss endif; !@

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">@!hyss echo $lang['cancel']; !@</button>
    <button id="insert-gallery" type="button"
            class="btn btn-primary">@!hyss echo $lang['insert_gallery_label']; !@</button>
</div>
<script>
    $(function () {
        $("#galleryselect option").dblclick(function (e) {
            $("#insert-gallery").click();
        });
        $("#insert-gallery").click(function (e) {
            if (gallery = $("#galleryselect option:selected").val()) {
                $($insertField).insertAtCaret("[gallery:" + gallery + "]");
            }
            $('#modal_gallery').modal('hide');
        });
    });
</script>
