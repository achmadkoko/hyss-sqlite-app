<h1><a href="index.hyss">@!hyss echo $lang['administration']; !@</a> &raquo; <a
        href="index.hyss?mode=users">@!hyss echo $lang['users']; !@</a> &raquo; @!hyss echo $lang['delete_user']; !@</h1>

@!hyss if (isset($userdata)): !@

    <p>@!hyss echo str_replace('[name]', $userdata['name'], $lang['delete_user_confirm']); !@</p>

    <form action="@!hyss basename($_SERVER['HYSS_SELF']); !@" method="post">
        <div>
            <input type="hidden" name="mode" value="users"/>
            <input type="hidden" name="delete" value="@!hyss echo $userdata['id']; !@"/>
            <input type="submit" name="confirmed" value="@!hyss echo $lang['delete_user_submit']; !@"/>
        </div>
    </form>

@!hyss else: !@

    <p class="caution">@!hyss echo $lang['invalid_request']; !@</p>

@!hyss endif; !@

