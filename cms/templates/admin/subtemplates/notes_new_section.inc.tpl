<ol class="breadcrumb">
    <li><a href="index.hyss?mode=notes">@!hyss echo $lang['notes']; !@</a></li>
    <li class="active">@!hyss echo $lang['create_note_section']; !@</li>
</ol>

<h1>@!hyss echo $lang['create_note_section']; !@</h1>

@!hyss include(BASE_PATH.'cms/templates/admin/subtemplates/errors.inc.tpl'); !@

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="notes"/>

        <label for="new_note_section">@!hyss echo $lang['note_section_name_m']; !@</label>

        <div class="input-group form-control-default">
            <input class="form-control" id="new_note_section" type="text" name="new_note_section"
                   value="@!hyss if (isset($new_note_section)) echo stripslashes($new_note_section); !@" size="25"/>
            <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">@!hyss echo $lang['submit_button_ok']; !@</button>
            </span>
        </div>
</form>

