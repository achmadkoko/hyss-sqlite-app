<ol class="breadcrumb">
    <li><a href="index.hyss?mode=comments">@!hyss echo $lang['comments']; !@</a></li>
    <li class="active">@!hyss echo $lang['delete_comments']; !@</li>
</ol>

<h1>@!hyss echo $lang['delete_comments']; !@</h1>

<p>@!hyss echo $lang['delete_checked_confirm']; !@</p>

<ul>
    @!hyss foreach ($comments as $comment): !@
        <li><strong>@!hyss echo $comment['name']; !@:</strong> @!hyss echo $comment['comment']; !@</li>
    @!hyss endforeach; !@
</ul>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="comments"/>
        <input type="hidden" name="type" value="@!hyss echo $type; !@"/>
        <input type="hidden" name="page" value="@!hyss echo $page; !@"/>
        @!hyss foreach ($comments as $comment): !@
            <input type="hidden" name="checked_ids_confirmed[]" value="@!hyss echo $comment['id']; !@"/>
        @!hyss endforeach; !@
        <input class="btn btn-danger btn-lg" type="submit" name="delete_checked_confirmed"
               value="@!hyss echo $lang['delete_checked_confirm_subm']; !@"/>
    </div>
</form>
