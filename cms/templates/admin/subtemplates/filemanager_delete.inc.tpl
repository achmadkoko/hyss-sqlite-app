<h1><a href="index.hyss">@!hyss echo $lang['administration']; !@</a> &raquo; <a
        href="index.hyss?mode=filemanager">@!hyss echo $lang['filemanager']; !@</a> &raquo; @!hyss echo $lang['delete_file']; !@
</h1>

<p>@!hyss echo str_replace('[file]', $file, $lang['delete_file_confirm']); !@</p>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="filemanager"/>
        <input type="hidden" name="delete" value="@!hyss echo $file !@"/>
        <input type="hidden" name="dir" value="@!hyss echo $directory; !@"/>
        <input type="submit" name="confirmed" value="@!hyss echo $lang['delete_file_submit']; !@">
    </div>
</form>

