<div class="row">
    <div class="col-md-10">
        <h1><h1>@!hyss echo $lang['page_overview']; !@</h1></h1>
    </div>
    <div class="col-md-2">
        @!hyss if ($user_type == 1): !@
        <a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=edit" class="btn btn-success btn-top pull-right">
            <span class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['admin_menu_new_page']; !@
            </a>@!hyss endif; !@
    </div>
</div>

@!hyss if (isset($pages)): !@
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>
                    <a href="index.hyss?mode=pages&amp;order=page&amp;descasc=@!hyss if ($descasc == "ASC" && $order == "page"): !@DESC@!hyss else: !@ASC@!hyss endif; !@">@!hyss echo $lang['page_name_marking']; !@@!hyss if ($order == "page" && $descasc == "ASC"): !@
                            <span
                                class="glyphicon glyphicon-chevron-down"></span>@!hyss elseif ($order == "page" && $descasc == "DESC"): !@
                            <span class="glyphicon glyphicon-chevron-up"></span>@!hyss endif; !@</a></th>
                <th>
                    <a href="index.hyss?mode=pages&amp;order=title&amp;descasc=@!hyss if ($descasc == "ASC" && $order == "title"): !@DESC@!hyss else: !@ASC@!hyss endif; !@">@!hyss echo $lang['title_marking']; !@@!hyss if ($order == "title" && $descasc == "ASC"): !@
                            <span
                                class="glyphicon glyphicon-chevron-down"></span>@!hyss elseif ($order == "title" && $descasc == "DESC"): !@
                            <span class="glyphicon glyphicon-chevron-up"></span>@!hyss endif; !@</a></th>
                <th>
                    <a href="index.hyss?mode=pages&amp;order=time&amp;descasc=@!hyss if ($descasc == "ASC" && $order == "time"): !@DESC@!hyss else: !@ASC@!hyss endif; !@">@!hyss echo $lang['created_marking']; !@@!hyss if ($order == "time" && $descasc == "ASC"): !@
                            <span
                                class="glyphicon glyphicon-chevron-down"></span>@!hyss elseif ($order == "time" && $descasc == "DESC"): !@
                            <span class="glyphicon glyphicon-chevron-up"></span>@!hyss endif; !@</a></th>
                <th>
                    <a href="index.hyss?mode=pages&amp;order=last_modified&amp;descasc=@!hyss if ($descasc == "ASC" && $order == "last_modified"): !@DESC@!hyss else: !@ASC@!hyss endif; !@">@!hyss echo $lang['last_modified_marking']; !@@!hyss if ($order == "last_modified" && $descasc == "ASC"): !@
                            <span
                                class="glyphicon glyphicon-chevron-down"></span>@!hyss elseif ($order == "last_modified" && $descasc == "DESC"): !@
                            <span class="glyphicon glyphicon-chevron-up"></span>@!hyss endif; !@</a></th>
                @!hyss if ($settings['count_views']): !@
                    <th>
                        <a href="index.hyss?mode=pages&amp;order=views&amp;descasc=@!hyss if ($descasc == "ASC" && $order == "views"): !@DESC@!hyss else: !@ASC@!hyss endif; !@">@!hyss echo $lang['views_marking']; !@@!hyss if ($order == "views" && $descasc == "ASC"): !@
                                <span
                                    class="glyphicon glyphicon-chevron-down"></span>@!hyss elseif ($order == "views" && $descasc == "DESC"): !@
                                <span class="glyphicon glyphicon-chevron-up"></span>@!hyss endif; !@</a></th>
                @!hyss endif; !@
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @!hyss $i = 0;
            $total_views = 0;
            foreach ($pages as $page): !@
                <tr>
                    <td>@!hyss if ($page['status'] > 0) { !@
                            <a
                            href="@!hyss echo BASE_URL . $page['page']; !@">@!hyss if ($settings['index_page'] == $page['page']) { !@
                            <b>@!hyss
                        }
                            echo $page['page'];
                        if ($settings['index_page'] == $page['page']) {
                            !@</b>@!hyss } !@
                            </a>@!hyss } else echo $page['page']; !@</td>
                    <td>@!hyss echo $page['title']; !@</td>
                    <td class="nowrap">@!hyss echo strftime($lang['time_format'], $page['time']);
                        if (isset($users[$page['author']])) {
                            !@ <span class="smallx">
                            (@!hyss echo $users[$page['author']]; !@)</span>@!hyss } !@</td>
                    <td class="nowrap">@!hyss echo strftime($lang['time_format'], $page['last_modified']);
                        if (isset($users[$page['last_modified_by']])) {
                            !@ <span class="smallx">
                            (@!hyss echo $users[$page['last_modified_by']]; !@)</span>@!hyss } !@</td>
                    @!hyss if ($settings['count_views']): !@
                        <td>@!hyss echo $page['views'];
                            $total_views = $total_views + $page['views']; !@</td>
                    @!hyss endif; !@
                    @!hyss if ($page['edit_permission']): !@
                        <td class="options nowrap"><a href="index.hyss?mode=edit&amp;id=@!hyss echo $page['id']; !@"
                                                      title="@!hyss echo $lang['edit']; !@"
                                                      class="btn btn-primary btn-xs">
                                <span class="glyphicon glyphicon-pencil"></span></a>&nbsp;
                            <a href="index.hyss?mode=pages&amp;delete_page=@!hyss echo $page['id']; !@"
                               title="@!hyss echo $lang['delete']; !@" class="btn btn-danger btn-xs"
                               onclick="str='@!hyss echo rawurlencode($lang['delete_page_confirm']); !@'; return confirm_link(str.replace('@!hyss echo rawurlencode('[page]'); !@','@!hyss echo rawurlencode($page['page']); !@'),this)"><span
                                    class="glyphicon glyphicon-remove"></span></a></td>
                    @!hyss else: !@
                        <td>&nbsp;</td>
                    @!hyss endif; !@
                </tr>
                @!hyss $i++; endforeach; !@
            @!hyss if ($settings['count_views']): !@
                <tr>
                    <td colspan="4"
                        style="text-align:right;">@!hyss echo str_replace('[time]', strftime($lang['time_format'], $settings['counter_last_resetted']), $lang['total_views']); !@</td>
                    <td><b>@!hyss echo $total_views; !@</b></td>
                    <td colspan="2">@!hyss if ($_SESSION[$settings['session_prefix'] . 'user_type'] == 1) { !@<span
                            class="small">
                            <a href="@!hyss echo basename($_SERVER['HYSS_SELF']); !@?mode=pages&amp;reset_views=true">@!hyss echo $lang['reset_views']; !@</a>
                            </span>@!hyss } else { !@&nbsp;@!hyss } !@</td>
                </tr>
            @!hyss endif; !@
            </tbody>
        </table>
    </div>
@!hyss else: !@
    <div class="alert alert-info">@!hyss echo $lang['no_pages']; !@</div>
@!hyss endif; !@
