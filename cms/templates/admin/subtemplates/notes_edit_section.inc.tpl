<ol class="breadcrumb">
    <li><a href="index.hyss?mode=notes">@!hyss echo $lang['notes']; !@</a></li>
    <li class="active">@!hyss echo $lang['notes']; !@: @!hyss echo $note_section; !@</li>
</ol>

<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo $lang['notes']; !@: @!hyss echo $note_section; !@</h1>
    </div>
    <div class="col-md-2">
        <a class="btn btn-success btn-top pull-right"
           href="index.hyss?mode=notes&amp;add_note=@!hyss echo $note_section; !@">
            <span class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['add_note']; !@</a>
    </div>
</div>

@!hyss if (isset($notes)): !@

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <tbody data-sortable="@!hyss echo BASE_URL; !@cms/?mode=notes&amp;reorder_notes=true">
            @!hyss $i = 0;
            foreach ($notes as $note): !@
                <tr id="item_@!hyss echo $note['id']; !@">
                    <td class="@!hyss if ($i % 2 == 0): !@a@!hyss else: !@b@!hyss endif; !@" style="cursor:move;">
                        <h3>@!hyss echo stripslashes($note['title']); !@</h3>

                        <p>@!hyss echo $note['text']; !@
                            @!hyss if ($note['link'] != ''): !@<br/>
                                <a
                                href="@!hyss echo $note['link']; !@">@!hyss echo $note['linkname']; !@</a>@!hyss endif; !@
                        </p>
                    </td>
                    <td class="options nowrap">
                        <a class="btn btn-primary btn-xs"
                           href="index.hyss?mode=notes&amp;edit_note=@!hyss echo $note['id']; !@"
                           title="@!hyss echo $lang['edit']; !@">
                            <span class="glyphicon glyphicon-pencil"></span></a>
                        <a class="btn btn-danger btn-xs"
                           href="index.hyss?mode=notes&amp;delete_note=@!hyss echo $note['id']; !@"
                           title="@!hyss echo $lang['delete']; !@"
                           data-delete-confirm="@!hyss echo rawurlencode($lang['delete_note_confirm']); !@">
                            <span class="glyphicon glyphicon-remove"></span></a>
                        <span class="btn btn-success btn-xs sortable-handle"
                              title="@!hyss echo $lang['drag_and_drop']; !@"><span
                                class="glyphicon glyphicon-sort"></span></span><!--<a href="index.hyss?mode=notes&amp;move_up=@!hyss echo $note['id']; !@"><img src="@!hyss echo BASE_URL; !@templates/admin/images/arrow_up.png" alt="@!hyss echo $lang['move_up']; !@" title="@!hyss echo $lang['move_up']; !@" width="16" height="16" /></a><a href="index.hyss?mode=notes&amp;move_down=@!hyss echo $note['id']; !@"><img src="@!hyss echo BASE_URL; !@templates/admin/images/arrow_down.png" alt="@!hyss echo $lang['move_up']; !@" title="@!hyss echo $lang['move_up']; !@" width="16" height="16" /></a>-->
                    </td>
                </tr>
                @!hyss $i++; endforeach; !@
            </tbody>
        </table>
    </div>

@!hyss else: !@

    <div class="alert alert-warning">@!hyss echo $lang['no_notes']; !@</div>

@!hyss endif; !@
