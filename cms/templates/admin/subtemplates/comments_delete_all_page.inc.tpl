<ol class="breadcrumb">
    <li><a href="index.hyss?mode=comments">@!hyss echo $lang['comments']; !@</a></li>
    <li class="active">@!hyss echo $lang['delete_comments']; !@</li>
</ol>

<h1>@!hyss echo $lang['delete_comments']; !@</h1>

<p>@!hyss echo str_replace('[page]', $page, $lang['delete_all_comm_page_conf']); !@</p>

<form action="@!hyss echo basename($_SERVER['HYSS_SELF']); !@" method="post">
    <div>
        <input type="hidden" name="mode" value="comments"/>
        <input type="hidden" name="type" value="@!hyss echo $type; !@"/>
        <input type="hidden" name="comment_id" value="@!hyss echo $comment_id; !@"/>
        <input class="btn btn-danger btn-lg" type="submit" name="delete_all_comments_page_confirmed"
               value="@!hyss echo $lang['delete_all_comments_subm']; !@"/>
    </div>
</form>
