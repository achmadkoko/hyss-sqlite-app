@!hyss if (isset($errors)): !@
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h3><span class="glyphicon glyphicon-warning-sign"></span>
            <strong>@!hyss echo $lang['error_headline']; !@</strong></h3>
        <ul>
            @!hyss foreach ($errors as $error): !@
                <li>@!hyss if (isset($lang[$error])) echo $lang[$error]; else echo $error; !@</li>
            @!hyss endforeach; !@
        </ul>
    </div>
@!hyss endif; !@
