<ul class="breadcrumb">
    <li><a href="index.hyss?mode=comments">@!hyss echo $lang['comments']; !@</a></li>
    <li class="active">@!hyss echo $lang['report_spam']; !@</li>
</ul>

<h1>@!hyss echo $lang['report_spam']; !@</h1>

<p>@!hyss echo $lang['report_spam_confirm']; !@</p>

<div class="panel panel-default">
    <div class="panel-heading"><strong>@!hyss echo $comment['name']; !@
            ,</strong> @!hyss echo strftime($lang['time_format'], $comment['time']); !@</div>
    <div class="panel-body">@!hyss echo $comment['comment']; !@</div>
</div>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="comments"/>
        <input type="hidden" name="type" value="@!hyss echo $type; !@"/>
        <input type="hidden" name="id" value="@!hyss echo $comment['id']; !@"/>
        <input type="hidden" name="comment_id" value="@!hyss echo $comment_id; !@"/>
        <input type="hidden" name="page" value="@!hyss echo $page; !@"/>
        <input class="btn btn-danger" type="submit" name="report_as_spam"
               value="@!hyss echo $lang['report_as_spam_submit']; !@"/> <input class="btn btn-danger" type="submit"
                                                                              name="report_as_spam_and_delete"
                                                                              value="@!hyss echo $lang['report_as_spam_delete_submit']; !@"/>
    </div>
</form>

