<h1><a href="index.hyss">@!hyss echo $lang['administration']; !@</a> &raquo; <a
        href="index.hyss?mode=galleries">@!hyss echo $lang['photo_galleries']; !@</a> &raquo; <a
        href="index.hyss?mode=galleries&amp;edit=@!hyss echo $gallery; !@">@!hyss echo str_replace('[gallery]', $gallery, $lang['edit_gallery']); !@</a> &raquo; @!hyss echo $lang['gallery_properties']; !@
</h1>

<form action="index.hyss" method="post">
    <div>
        <div>
            <input type="hidden" name="mode" value="galleries"/>
            <input type="hidden" name="gallery" value="@!hyss echo $gallery_data['gallery']; !@"/>
            <table class="admin-table" cellspacing="1" cellpadding="5" border="0">
                <tr>
                    <td class="c"><label for="template">@!hyss echo $lang['specify_photo_tpl_m']; !@</label></td>
                    <td class="b">
                        <select id="template" name="template" size="1">
                            @!hyss foreach ($available_photo_templates as $photo_template): !@
                                <option
                                    value="@!hyss echo $photo_template; !@"@!hyss if (isset($gallery_data['template']) && $gallery_data['template'] == $photo_template): !@ selected="selected"@!hyss endif; !@>@!hyss echo $photo_template; !@</option>
                            @!hyss endforeach; !@
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="c"><label for="photos_per_row">@!hyss echo $lang['photos_per_row']; !@</label></td>
                    <td class="b"><input type="text" id="photos_per_row" name="photos_per_row"
                                         value="@!hyss echo $gallery_data['photos_per_row']; !@" size="5"/></td>
                </tr>
                <tr>
                    <td class="c">&nbsp;</td>
                    <td class="b" style="text-align:right;"><input type="submit" name="gallery_properties_submit"
                                                                   value="@!hyss echo $lang['submit_button_ok']; !@"/>
                    </td>
                </tr>
            </table>
        </div>
</form>
