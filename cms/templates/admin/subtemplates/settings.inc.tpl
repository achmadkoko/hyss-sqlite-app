<h1>@!hyss echo $lang['settings']; !@</h1>

@!hyss if (isset($saved)): !@
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <span
            class="glyphicon glyphicon-ok"></span> @!hyss echo $lang['settings_saved']; !@@!hyss if (isset($cache_cleared)): !@ / @!hyss echo $lang['cache_cleared']; !@@!hyss endif; !@
    </div>
@!hyss endif; !@

<form action="@!hyss echo BASE_URL; !@cms/index.hyss" method="post" class="form-horizontal">
    <input type="hidden" name="mode" value="settings"/>
    <input type="hidden" name="settings_submitted"/>

    <div class="form-group">
        <label for="website_title" class="col-md-2 control-label">@!hyss echo $lang['settings_website_title']; !@</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="website_title" name="website_title"
                   value="@!hyss echo htmlspecialchars($settings['website_title']); !@" size="35">
        </div>
    </div>

    <div class="form-group">
        <label for="website_subtitle"
               class="col-md-2 control-label">@!hyss echo $lang['settings_website_subtitle']; !@</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="website_subtitle" name="website_subtitle"
                   value="@!hyss echo htmlspecialchars($settings['website_subtitle']); !@" size="35">
        </div>
    </div>

    <div class="form-group">
        <label for="author" class="col-md-2 control-label">@!hyss echo $lang['settings_author']; !@</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="author" name="author"
                   value="@!hyss echo htmlspecialchars($settings['author']); !@" size="35">
        </div>
    </div>

    <div class="form-group">
        <label for="email" class="col-md-2 control-label">@!hyss echo $lang['settings_email']; !@</label>

        <div class="col-md-6">
            <input type="text" class="form-control" id="email" name="email"
                   value="@!hyss echo htmlspecialchars($settings['email']); !@" size="35">
        </div>
    </div>

    <div class="form-group">
        <label for="index_page" class="col-md-2 control-label">@!hyss echo $lang['settings_index_page']; !@</label>

        <div class="col-md-6">
            <select id="index_page" name="index_page" size="1" class="form-control">
                @!hyss foreach ($pages as $current_page): !@
                    <option
                        value="@!hyss echo $current_page['page']; !@"@!hyss if ($settings['index_page'] == $current_page['page']): !@ selected="selected"@!hyss endif; !@>@!hyss echo $current_page['page']; !@</option>
                @!hyss endforeach; !@
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="error_page" class="col-md-2 control-label">@!hyss echo $lang['settings_error_page']; !@</label>

        <div class="col-md-6">
            <select id="error_page" name="error_page" size="1" class="form-control">
                @!hyss foreach ($pages as $current_page): !@
                    <option
                        value="@!hyss echo $current_page['page']; !@"@!hyss if ($settings['error_page'] == $current_page['page']): !@ selected="selected"@!hyss endif; !@>@!hyss echo $current_page['page']; !@</option>
                @!hyss endforeach; !@
            </select></div>
    </div>

    <div class="form-group">
        <label for="admin_language" class="col-md-2 control-label">@!hyss echo $lang['admin_language']; !@</label>

        <div class="col-md-6">
            <select id="admin_language" name="admin_language" size="1" class="form-control">
                @!hyss foreach ($admin_languages as $admin_language): !@
                    <option
                        value="@!hyss echo $admin_language['identifier']; !@"@!hyss if ($settings['admin_language'] == $admin_language['identifier']): !@ selected@!hyss endif !@>@!hyss echo $admin_language['name']; !@</option>
                @!hyss endforeach; !@
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="default_page_language"
               class="col-md-2 control-label">@!hyss echo $lang['default_page_language']; !@</label>

        <div class="col-md-6">
            <select id="default_page_language" name="default_page_language" size="1" class="form-control">
                @!hyss foreach ($page_languages as $page_language): !@
                    <option
                        value="@!hyss echo $page_language['identifier']; !@"@!hyss if ($settings['default_page_language'] == $page_language['identifier']): !@ selected@!hyss endif !@>@!hyss echo $page_language['name']; !@</option>
                @!hyss endforeach; !@
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">

            <div class="checkbox">
                <label for="caching">
                    <input id="caching" type="checkbox" name="caching"
                           value="1"@!hyss if ($settings['caching'] == 1): !@ checked@!hyss endif; !@> @!hyss echo $lang['settings_caching_enabled']; !@
                </label>
            </div>

            @!hyss if ($settings['caching'] && empty($settings['admin_auto_clear_cache'])): !@
                <div class="checkbox">
                    <label for="clear_cache">
                        <input id="clear_cache" type="checkbox" name="clear_cache"
                               value="1"> @!hyss echo $lang['admin_menu_clear_cache']; !@
                    </label>
                </div>
            @!hyss endif; !@

        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-6">
            <button type="submit" class="btn btn-primary btn-strong">@!hyss echo $lang['submit_button_ok']; !@</button>
            <a class="btn btn-default"
               href="index.hyss?mode=settings&amp;action=advanced_settings">@!hyss echo $lang['advanced_settings']; !@</a>
        </div>
    </div>

</form>
