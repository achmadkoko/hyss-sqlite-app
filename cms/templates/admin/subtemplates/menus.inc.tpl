<div class="row">
    <div class="col-sm-11">
        <h1>@!hyss echo $lang['menus']; !@</h1>
    </div>
    <div class="col-sm-1">
        <a class="btn btn-success btn-top pull-right" href="index.hyss?mode=menus&amp;action=new"><span
                class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['create_menu']; !@</a>
    </div>
</div>

@!hyss if (isset($menus)): !@

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>@!hyss echo $lang['menu']; !@</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @!hyss $i = 0;
            foreach ($menus as $menu): !@
                <tr>
                    <td>@!hyss echo $menu; !@</td>
                    <td class="options nowrap"><a class="btn btn-primary btn-xs"
                                                  href="index.hyss?mode=menus&amp;edit=@!hyss echo $menu; !@"
                                                  title="@!hyss echo $lang['edit']; !@"><span
                                class="glyphicon glyphicon-pencil"></a>
                        <a class="btn btn-danger btn-xs" href="index.hyss?mode=menus&amp;delete=@!hyss echo $menu; !@"
                           title="@!hyss echo $lang['delete']; !@"
                           data-delete-confirm="@!hyss echo rawurlencode($lang['delete_menu_confirm']); !@"><span
                                class="glyphicon glyphicon-remove"></a>
                        @!hyss if ($menu == $settings['default_menu']): !@<span class="btn btn-success btn-xs"
                                                                               title="@!hyss echo $lang['default_menu']; !@">
                                <span class="glyphicon glyphicon-check"></span></span>@!hyss else: !@<a
                            class="btn btn-default btn-xs"
                            href="index.hyss?mode=menus&amp;set_default=@!hyss echo $menu; !@"
                            title="@!hyss echo $lang['set_default_menu']; !@"><span
                                    class="glyphicon glyphicon-unchecked"></span></a>@!hyss endif; !@</td>
                </tr>
                @!hyss ++$i; endforeach; !@
            </tbody>
        </table>
    </div>

@!hyss else: !@

    <p>@!hyss echo $lang['no_menu']; !@</p>

@!hyss endif; !@
