<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo $lang['photo_galleries']; !@</h1>
    </div>
    <div class="col-md-2">
        <a class="btn btn-success btn-top pull-right" href="index.hyss?mode=galleries&amp;action=new"><span
                class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['create_new_gallery']; !@</a>
    </div>
</div>

@!hyss if (isset($galleries)): !@

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>@!hyss echo $lang['gallery']; !@</th>
                <th colspan="2">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @!hyss $i = 0;
            foreach ($galleries as $gallery): !@
                <tr>
                    <td>@!hyss echo htmlspecialchars(stripslashes($gallery)); !@</td>
                    <td class="options"><a href="index.hyss?mode=galleries&amp;edit=@!hyss echo $gallery; !@"
                                           title="@!hyss echo $lang['edit']; !@" class="btn btn-primary btn-xs"><span
                                class="glyphicon glyphicon-pencil"></span></a>&nbsp; <a
                            href="index.hyss?mode=galleries&amp;delete_gallery=@!hyss echo $gallery; !@"
                            data-delete-confirm="@!hyss echo rawurlencode($lang['delete_gallery_confirm']); !@"
                            title="@!hyss echo $lang['delete']; !@" class="btn btn-danger btn-xs"><span
                                class="glyphicon glyphicon-remove"></span></a></td>
                </tr>
                @!hyss $i++; endforeach; !@
            </tbody>
        </table>
    </div>

@!hyss else: !@

    <div class="alert alert-warning">
        @!hyss echo $lang['no_gallery']; !@
    </div>

@!hyss endif; !@
