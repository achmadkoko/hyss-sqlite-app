<ol class="breadcrumb">
    <li><a href="index.hyss?mode=comments">@!hyss echo $lang['comments']; !@</a></li>
    <li class="active">@!hyss echo $lang['delete_comments']; !@</li>
</ol>

<h1>@!hyss echo $lang['delete_comments']; !@</h1>

@!hyss if ($type == 0): !@
    <p>@!hyss echo $lang['delete_all_page_comments']; !@</p>
@!hyss else: !@
    <p>@!hyss echo $lang['delete_all_photo_comments']; !@</p>
@!hyss endif !@

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="comments"/>
        <input type="hidden" name="type" value="@!hyss echo $type; !@"/>
        <input class="btn btn-danger btn-lg" type="submit" name="delete_all_comments_confirmed"
               value="@!hyss echo $lang['delete_all_comments_subm']; !@"/>
    </div>
</form>
