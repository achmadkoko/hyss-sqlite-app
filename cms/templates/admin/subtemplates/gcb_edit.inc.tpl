<ol class="breadcrumb">
    <li><a href="index.hyss?mode=gcb">@!hyss echo $lang['gcb']; !@</a></li>
    <li class="active">@!hyss if (isset($gcb['id'])): echo $lang['edit_gcb'];
        else: echo $lang['add_gcb']; endif; !@</li>
</ol>

<h1>@!hyss if (isset($gcb['id'])): echo $lang['edit_gcb'];
    else: echo $lang['add_gcb']; endif; !@</h1>

@!hyss include(BASE_PATH.'cms/templates/admin/subtemplates/errors.inc.tpl'); !@

<form action="index.hyss" method="post">
    <input type="hidden" name="mode" value="gcb"/>
    @!hyss if (isset($gcb['id'])): !@
        <input type="hidden" name="id" value="@!hyss echo $gcb['id']; !@"/>
    @!hyss endif; !@

    <div class="form-group">
        <label for="identifier" class="control-label">@!hyss echo $lang['edit_gcb_identifier']; !@</label>
        <input type="text" name="identifier" value="@!hyss if (isset($gcb['identifier'])) echo $gcb['identifier']; !@"
               size="40" class="form-control"/>
    </div>

    <div class="form-group">
        <label for="content" class="control-label">@!hyss echo $lang['edit_gcb_content']; !@</label>
        <textarea name="content" cols="70" rows="20"
                  class="form-control">@!hyss if (isset($gcb['content'])) echo $gcb['content']; !@</textarea>
    </div>

    <div class="form-group">
        <input type="submit" name="edit_gcb_submit" value="@!hyss echo $lang['submit_button_ok']; !@"
               class="btn btn-primary btn-strong"/>
    </div>

</form>
