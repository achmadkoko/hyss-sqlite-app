<ol class="breadcrumb">
    <li><a href="index.hyss?mode=notes">@!hyss echo $lang['notes']; !@</a></li>
    <li class="active">@!hyss echo $lang['delete_note_section']; !@</li>
</ol>

<h1>@!hyss echo $lang['delete_note_section']; !@</h1>

<p>@!hyss echo str_replace('[note_section]', $note_section, $lang['delete_note_section_confirm']); !@</p>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="notes"/>
        <input type="hidden" name="delete" value="@!hyss echo $note_section; !@"/>
        <input class="btn btn-danger btn-lg" type="submit" name="confirmed"
               value="@!hyss echo $lang['delete_note_section_submit']; !@"/>
    </div>
</form>
