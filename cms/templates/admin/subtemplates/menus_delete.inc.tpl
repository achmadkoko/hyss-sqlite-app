<h1>@!hyss echo $lang['delete_menu_headline']; !@</h1>

<p>@!hyss echo $lang['delete_menu_confirm']; !@</p>
<p>@!hyss echo $lang['delete_menu_name']; !@<b> @!hyss echo $menu; !@</b></p>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="menus"/>
        <input type="hidden" name="delete" value="@!hyss echo $menu; !@"/>
        <input type="submit" name="confirmed" value="@!hyss echo $lang['delete_menu_submit']; !@"/>
    </div>
</form>

