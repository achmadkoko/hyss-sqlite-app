<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">@!hyss echo $lang['insert_thumbnail']; !@</h4>
</div>
<div class="modal-body">
    @!hyss if (isset($thumbnails)): !@
        <div class="form-group">
            <select id="thumbnailselect" class="form-control" size="20">
                @!hyss foreach ($thumbnails as $thumbnail): !@
                    
                    @!hyss
                    $previous_gallery = isset($current_gallery) ? $current_gallery : false;
                    $current_gallery = $thumbnail['gallery'];
                    !@
                    
                    @!hyss if($current_gallery!=$previous_gallery): !@
                    @!hyss if($previous_gallery): !@
                    	</optgroup>
                    @!hyss endif; !@
                    	<optgroup label="@!hyss echo $current_gallery; !@">
                    @!hyss endif; !@
                    
                    <option value="@!hyss echo $thumbnail['id']; !@">
                    	@!hyss echo $thumbnail['title']; !@
                    </option>
                    
                    @!hyss if($current_gallery!=$previous_gallery): !@
                    
                    @!hyss endif; !@                    
                    
                @!hyss endforeach; !@
                </optgroup>            </select>
        </div>
        <div class="form-group">
            <label for="image_class">@!hyss echo $lang['insert_image_class']; !@</label>
            <input id="image_class" class="form-control" type="text" name="image_class"
                   value="@!hyss echo $settings['default_thumbnail_class']; !@">
        </div>
    @!hyss else: !@
        <div class="alert alert-warning">
            <p>@!hyss echo $lang['no_images']; !@</p>
        </div>
    @!hyss endif; !@

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">@!hyss echo $lang['cancel']; !@</button>
    @!hyss if (isset($thumbnails)): !@
        <button id="insert-image" type="button"
                class="btn btn-primary">@!hyss echo $lang['insert_thumbnail_button']; !@</button>
    @!hyss endif; !@
</div>
<script>
    $(function () {
        $("#thumbnailselect option").dblclick(function (e) {
            $("#insert-image").click();
        });
        $("#insert-image").click(function (e) {
            if (thumbnail = $("#thumbnailselect option:selected").val()) {
                if (imageClass = $("#image_class").val()) imageCode = "[thumbnail:" + thumbnail + "|" + imageClass + "]";
                else imageCode = "[image:" + thumbnail + "]";
                $($insertField).insertAtCaret(imageCode);
            }
            $('#modal_thumbnail').modal('hide');
        });
    });
</script>
