<ol class="breadcrumb">
    <li><a href="index.hyss?mode=galleries">@!hyss echo $lang['photo_galleries']; !@</a></li>
    <li class="active">@!hyss echo $lang['new_gallery']; !@</li>
</ol>

<h1>@!hyss echo $lang['new_gallery']; !@</h1>

@!hyss include(BASE_PATH.'cms/templates/admin/subtemplates/errors.inc.tpl'); !@

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="galleries"/>

        <label for="new_gallery_name">@!hyss echo $lang['new_gallery_name']; !@</label>

        <div class="input-group form-control-default">
            <input class="form-control" id="new_gallery_name" type="text" name="new_gallery_name"
                   value="@!hyss if (isset($new_gallery_name)) echo $new_gallery_name; !@" size="25"/>
  <span class="input-group-btn">
  <button class="btn btn-primary" type="submit">@!hyss echo $lang['submit_button_ok']; !@</button>
  </span>
        </div>

    </div>
</form>
