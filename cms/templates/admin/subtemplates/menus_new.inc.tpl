<ol class="breadcrumb">
    <li><a href="index.hyss?mode=menus">@!hyss echo $lang['menus']; !@</a></a></li>
    <li class="active">@!hyss echo $lang['new_menu_hl']; !@</li>
</ol>

<h1>@!hyss echo $lang['new_menu_hl']; !@</h1>

@!hyss include(BASE_PATH.'cms/templates/admin/subtemplates/errors.inc.tpl'); !@

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="menus"/>
        <label for="new_menu_name">@!hyss echo $lang['new_menu_name']; !@</label>

        <div class="input-group form-control-default">
            <input class="form-control" id="new_menu_name" type="text" name="new_menu_name"
                   value="@!hyss if (isset($new_menu_name)) echo $new_menu_name; !@" size="25"/>
            <span class="input-group-btn">
            <button class="btn btn-primary" type="submit">@!hyss echo $lang['submit_button_ok']; !@</button>
            </span>
        </div>
    </div>
</form>

