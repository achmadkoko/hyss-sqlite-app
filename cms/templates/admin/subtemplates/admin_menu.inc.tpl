<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="@!hyss echo BASE_URL; !@">@!hyss echo $settings['website_title']; !@</a>
        </div>
        <div class="navbar-collapse collapse">
            @!hyss if ($admin): !@
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="@!hyss echo BASE_URL; !@cms/" class="dropdown-toggle" data-toggle="dropdown"><span
                                class="glyphicon glyphicon-cog"></span> @!hyss echo $lang['admin_menu_admin']; !@ <b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=pages"><span
                                        class="glyphicon glyphicon-file"></span> @!hyss echo $lang['admin_menu_page_overview']; !@
                                </a></li>
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=edit"><span
                                        class="glyphicon glyphicon-plus-sign"></span> @!hyss echo $lang['admin_menu_new_page']; !@
                                </a></li>
                            <li class="divider"></li>
                            @!hyss if ($user_type == 1): !@
                                <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=settings"><span
                                        class="glyphicon glyphicon-wrench"></span> @!hyss echo $lang['admin_menu_settings']; !@
                                </a></li>@!hyss endif; !@
                            @!hyss if ($user_type == 1): !@
                                <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=menus"><span
                                        class="glyphicon glyphicon-list-alt"></span> @!hyss echo $lang['admin_menu_edit_menus']; !@
                                </a></li>@!hyss endif; !@
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=galleries"><span
                                        class="glyphicon glyphicon-picture"></span> @!hyss echo $lang['admin_menu_edit_galleries']; !@
                                </a></li>
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=comments"><span
                                        class="glyphicon glyphicon-comment"></span> @!hyss echo $lang['admin_menu_edit_comments']; !@
                                </a></li>
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=notes"><span
                                        class="glyphicon glyphicon-edit"></span> @!hyss echo $lang['admin_menu_edit_notes']; !@
                                </a></li>
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=gcb"><span
                                        class="glyphicon glyphicon-th-large"></span> @!hyss echo $lang['admin_menu_edit_gcb']; !@
                                </a></li>
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=filemanager"><span
                                        class="glyphicon glyphicon-folder-open"></span> @!hyss echo $lang['admin_menu_filemanager']; !@
                                </a></li>
                            @!hyss if ($user_type == 1): !@
                                <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=spam_protection"><span
                                        class="glyphicon glyphicon-ban-circle"></span> @!hyss echo $lang['admin_menu_spam_protection']; !@
                                </a></li>@!hyss endif; !@
                            <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=users"><span
                                        class="glyphicon glyphicon-user"></span> @!hyss if ($user_type == 1) echo $lang['admin_menu_user_administr']; else echo $lang['admin_menu_edit_userdata']; !@
                                </a></li>
                            @!hyss if ($settings['caching']): !@
                                <li class="divider"></li>
                                <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?clear_cache=true"><span
                                            class="glyphicon glyphicon-remove"></span> @!hyss echo $lang['admin_menu_clear_cache']; !@
                                    </a></li>
                            @!hyss endif; !@
                        </ul>
                    </li>
                </ul>
            @!hyss endif; !@
            @!hyss if ($admin): !@
                <ul class="nav navbar-nav navbar-right">
                    @!hyss if (defined('PAGE') && $authorized_to_edit): !@
                        <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=edit&amp;id=@!hyss echo $id; !@"><span
                                    class="glyphicon glyphicon-pencil"></span> @!hyss echo $lang['admin_menu_edit_page']; !@
                            </a></li>
                        <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=pages&amp;delete_page=@!hyss echo $id; !@"
                               onclick="return confirm_link('@!hyss echo rawurlencode($lang['admin_menu_delete_page_conf']); !@',this)"><span
                                    class="glyphicon glyphicon-remove"></span> @!hyss echo $lang['admin_menu_delete_page']; !@
                            </a></li>
                    @!hyss endif; !@
                    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=logout"><span
                                class="glyphicon glyphicon-off"></span> @!hyss echo $lang['admin_menu_logout']; !@</a>
                    </li>
                </ul>
            @!hyss endif; !@
        </div>
    </div>
</div>
