<ol class="breadcrumb">
    <li><a href="index.hyss?mode=gcb">@!hyss echo $lang['gcb']; !@</a></li>
    <li class="active">@!hyss echo $lang['delete_gcb']; !@</li>
</ol>

<h1>@!hyss echo $lang['delete_gcb']; !@</h1>

<p>@!hyss echo str_replace('[identifier]', $gcb['identifier'], $lang['delete_gcb_confirm']); !@</p>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="gcb"/>
        <input type="hidden" name="delete" value="@!hyss echo $gcb['id']; !@"/>
        <input type="submit" name="confirmed" value="@!hyss echo $lang['submit_button_delete']; !@"
               class="btn btn-danger btn-strong"/>
    </div>
</form>

