<ol class="breadcrumb">
    <li><a href="index.hyss?mode=galleries">@!hyss echo $lang['photo_galleries']; !@</a></li>
    <li class="active">@!hyss echo str_replace('[gallery]', $gallery, $lang['edit_gallery']); !@</li>
</ol>

<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo str_replace('[gallery]', $gallery, $lang['edit_gallery']); !@</h1>
    </div>
    <div class="col-md-2">
        <a class="btn btn-success btn-top pull-right"
           href="index.hyss?mode=galleries&amp;new_photo=@!hyss echo $gallery; !@"><span
                class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['add_photo']; !@</a>
    </div>
</div>

@!hyss if (isset($items)): !@

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>@!hyss echo $lang['photo']; !@</th>
            <th>@!hyss echo $lang['photo_title']; !@</th>
            <th>@!hyss echo $lang['photo_subtitle']; !@</th>
            <th>@!hyss echo $lang['photo_description']; !@</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody data-sortable="@!hyss echo BASE_URL; !@cms/?mode=galleries&amp;reorder_photos=true">
        @!hyss $i = 0;
        foreach ($items as $item): !@
            <tr id="item_@!hyss echo $item['id']; !@">
                <td><a class="thumbnail" href="@!hyss echo BASE_URL . MEDIA_DIR . $item['photo_normal']; !@"
                       data-lightbox><img id="photo@!hyss echo $item['id']; !@"
                                          src="@!hyss echo BASE_URL . MEDIA_DIR . $item['photo_thumbnail']; !@"
                                          title="@!hyss echo htmlspecialchars($item['title']); !@"
                                          alt="@!hyss echo htmlspecialchars($item['title']); !@"
                                          data-subtitle="@!hyss echo htmlspecialchars($item['subtitle']); !@"
                                          data-description="@!hyss echo htmlspecialchars($item['description']); !@"/></a>
                </td>
                <td>@!hyss echo $item['title']; !@</td>
                <td>@!hyss echo $item['subtitle']; !@</td>
                <td>@!hyss echo $item['description']; !@</td>
                <td class="nowrap"><a href="index.hyss?mode=galleries&amp;edit_photo=@!hyss echo $item['id']; !@"
                                      title="@!hyss echo $lang['edit']; !@" class="btn btn-primary btn-xs"><span
                            class="glyphicon glyphicon-pencil"></span></a>
                    <a href="index.hyss?mode=galleries&amp;delete_photo=@!hyss echo $item['id']; !@"
                       title="@!hyss echo $lang['delete']; !@"
                       data-delete-confirm="@!hyss echo rawurlencode($lang['delete_photo_confirm']); !@"
                       class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a>
                    <span class="btn btn-success btn-xs sortable-handle"
                          title="@!hyss echo $lang['drag_and_drop']; !@"><span
                            class="glyphicon glyphicon-sort"></span></span><!--<a href="index.hyss?mode=galleries&amp;move_up_photo=@!hyss echo $item['id']; !@#photo@!hyss echo $item['id']; !@"><img src="@!hyss echo BASE_URL; !@templates/admin/images/arrow_up.png" alt="@!hyss echo $lang['move_up']; !@" title="@!hyss echo $lang['move_up']; !@" width="16" height="16" /></a><a href="index.hyss?mode=galleries&amp;move_down_photo=@!hyss echo $item['id']; !@#photo@!hyss echo $item['id']; !@"><img src="@!hyss echo BASE_URL; !@templates/admin/images/arrow_down.png" alt="@!hyss echo $lang['move_down']; !@" title="@!hyss echo $lang['move_down']; !@" width="16" height="16" /></a>-->
                </td>
            </tr>
            @!hyss $i++; endforeach !@
        </tbody>
    </table>

@!hyss else: !@

    <div class="alert alert-warning">
        @!hyss echo $lang['no_photo']; !@
    </div>

@!hyss endif; !@
