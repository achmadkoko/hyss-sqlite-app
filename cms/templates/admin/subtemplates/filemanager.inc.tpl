<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo $lang['filemanager']; !@</h1>
    </div>
    <div class="col-md-2">
        <a class="btn btn-success btn-top pull-right"
           href="index.hyss?mode=filemanager&amp;action=upload&amp;directory=@!hyss echo $directory; !@"><span
                class="glyphicon glyphicon-upload"></span> @!hyss echo $lang['upload_file_link']; !@</a>
    </div>
</div>


<h1></h1>

<form class="form-inline" action="@!hyss echo basename($_SERVER['HYSS_SELF']); !@" method="get">
    <div class="form-group">
        <input type="hidden" name="mode" value="filemanager"/>
        <label for="directory">@!hyss echo $lang['directory']; !@</label> <select id="directory" class="form-control"
                                                                                 size="1" name="directory"
                                                                                 onchange="this.form.submit();">
            <option
                value="@!hyss echo $file_dir !@"@!hyss if ($directory == $file_dir): !@ selected="selected"@!hyss endif; !@>@!hyss echo $file_dir !@</option>
            <option
                value="@!hyss echo $media_dir !@"@!hyss if ($directory == $media_dir): !@ selected="selected"@!hyss endif; !@>@!hyss echo $media_dir !@</option>
        </select>
    </div>
</form>

@!hyss if (isset($files)): !@

<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>@!hyss echo $lang['file']; !@</th>
            @!hyss if (isset($mime_content_type)): !@
                <th>@!hyss echo $lang['file_type']; !@</th>@!hyss endif; !@
            <th>@!hyss echo $lang['file_size']; !@</th>
            <th>@!hyss echo $lang['file_date']; !@</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @!hyss $i = 0;
        foreach ($files as $file): !@

            <tr>
                <td>
                    <a href="@!hyss echo BASE_URL . 'static/' . $directory . '/' . $file['filename']; !@">@!hyss echo $file['filename']; !@</a>
                </td>
                @!hyss if (isset($mime_content_type)): !@
                    <td>@!hyss echo $file['mime_content_type']; !@</td>@!hyss endif; !@
                <td>@!hyss echo $file['size']; !@</td>
                <td>@!hyss echo $file['last_modified']; !@</td>
                <td class="options"><a class="btn btn-danger btn-xs"
                                       href="index.hyss?mode=filemanager&amp;directory=@!hyss echo $directory; !@&amp;delete=@!hyss echo $file['filename']; !@"
                                       title="@!hyss echo $lang['delete']; !@"
                                       data-delete-confirm="@!hyss echo rawurlencode($lang['delete_file_confirm']); !@"><span
                            class="glyphicon glyphicon-remove"></span></a></td>
            </tr>

            @!hyss ++$i; endforeach !@
        </tbody>
    </table>

    @!hyss else: !@

        <p><i>@!hyss echo $lang['no_files']; !@</i></p>

    @!hyss
    endif; !@
