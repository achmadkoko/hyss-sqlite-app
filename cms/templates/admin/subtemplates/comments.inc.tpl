<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo $lang['comments']; !@</h1>
    </div>
    <div class="col-md-2">
        <div id="itemnav">
            <form method="get" action="index.hyss">
                <div class="form-group">
                    <input type="hidden" name="mode" value="comments"/>
                    <input type="hidden" name="type" value="@!hyss echo $type; !@"/>
                    <select class="form-control form-control-medium btn-top pull-right" size="1" name="comment_id"
                            onchange="this.form.submit();">
                        <option
                            value="0">@!hyss if ($type == 0) echo $lang['comments_all_pages']; else echo $lang['comments_all_photos']; !@</option>
                        @!hyss foreach ($items as $key => $val): !@
                            <option
                                value="@!hyss echo $key; !@"@!hyss if ($key == $comment_id): !@ selected="selected"@!hyss endif; !@>@!hyss echo $val['title']; !@</option>
                        @!hyss endforeach; !@
                    </select><!--<input type="submit" name="" value="" src="@!hyss echo BASE_URL; !@templates/admin/images/submit.png" value="&raquo;" />
                    -->
                </div>
            </form>
        </div>
    </div>
</div>

<h1></h1>

@!hyss /*
<div id="nav">
 <ul id="navlist">
  <li><a @!hyss if($type==0): !@class="active" @!hyss endif; !@href="index.hyss?mode=comments&amp;type=0" style="width:140px;">@!hyss echo $lang['comments_page_c']; !@</a></li>
  <li><a @!hyss if($type==1): !@class="active" @!hyss endif; !@href="index.hyss?mode=comments&amp;type=1" style="width:140px;">@!hyss echo $lang['comments_photo_c']; !@</a></li>
 </ul>
<p>&nbsp;</p>
</div>
*/
!@

@!hyss if (isset($comments)): !@

    <form id="entryeditform" method="post" action="index.hyss">
        <div>
            <input type="hidden" name="mode" value="comments"/>

            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>@!hyss if ($type == 0) echo $lang['comments_page']; else echo $lang['comments_photo']; !@</th>
                        <th>@!hyss echo $lang['comments_time']; !@</th>
                        <th>@!hyss echo $lang['comments_name']; !@</th>
                        <th>@!hyss echo $lang['comments_comment']; !@</th>
                        <th>@!hyss echo $lang['comments_ip']; !@</th>
                        <th colspan="@!hyss if ($settings['akismet_key'] != '' && $settings['akismet_entry_check'] == 1): !@3@!hyss else: !@2@!hyss endif; !@">
                            &nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @!hyss $i = 0;
                    foreach ($comments as $comment): !@
                        <tr class="@!hyss if ($i % 2 == 0): !@a@!hyss else: !@b@!hyss endif; !@">
                            <td><input class="commentcheckbox" type="checkbox" name="checked[]"
                                       value="@!hyss echo $comment['id']; !@"/></td>
                            <td>@!hyss if ($type == 1): !@@!hyss if (isset($items[$comment['comment_id']])): !@<a
                                    href="@!hyss echo BASE_URL . MEDIA_DIR . $items[$comment['comment_id']]['photo_normal']; !@">
                                    <img
                                        src="@!hyss echo BASE_URL . MEDIA_DIR . $items[$comment['comment_id']]['photo_thumbnail']; !@"
                                        title="@!hyss echo $items[$comment['comment_id']]['title']; !@"
                                        alt="@!hyss echo $items[$comment['comment_id']]['title']; !@" />
                                    </a>@!hyss else: !@-@!hyss endif; !@@!hyss else: !@@!hyss if (isset($items[$comment['comment_id']])): !@
                                    <a
                                    href="@!hyss echo BASE_URL . $items[$comment['comment_id']]['page']; !@#comments">@!hyss echo $items[$comment['comment_id']]['title']; !@</a>@!hyss else: !@-@!hyss endif; !@@!hyss endif; !@
                            </td>
                            <td>@!hyss echo strftime($lang['time_format'], $comment['time']); !@</td>
                            <td>@!hyss if (isset($comment['email_hp'])): !@<a
                                    href="@!hyss echo $comment['email_hp']; !@">@!hyss echo $comment['name']; !@</a>@!hyss else: !@@!hyss echo $comment['name']; !@@!hyss endif; !@
                            </td>
                            <td>@!hyss if ($comment['comment'] == '' && $type == 0): !@
                                    <em>@!hyss echo $lang['pingback']; !@</em>@!hyss else: echo $comment['comment']; endif; !@
                            </td>
                            <td>@!hyss echo $comment['ip']; !@</td>
                            <td class="options"><a class="btn btn-primary btn-xs"
                                                   href="index.hyss?mode=comments&amp;type=@!hyss echo $type; !@&amp;edit=@!hyss echo $comment['id']; !@&amp;comment_id=@!hyss echo $comment_id; !@&amp;page=@!hyss echo $page; !@"
                                                   title="@!hyss echo $lang['edit']; !@"><span
                                        class="glyphicon glyphicon-pencil"></span></a>
                                <a class="btn btn-danger btn-xs"
                                   href="index.hyss?mode=comments&type=@!hyss echo $type; !@&amp;delete=@!hyss echo $comment['id']; !@&amp;comment_id=@!hyss echo $comment_id; !@&amp;page=@!hyss echo $page; !@"
                                   title="@!hyss echo $lang['delete']; !@"
                                   data-delete-confirm="@!hyss echo rawurlencode($lang['delete_this_comment_confirm']); !@"><span
                                        class="glyphicon glyphicon-remove"></span></a>
                                @!hyss if ($settings['akismet_key'] != '' && $settings['akismet_entry_check'] == 1): !@
                                    <a class="btn btn-danger btn-xs"
                                       href="index.hyss?mode=comments&type=@!hyss echo $type; !@&amp;report_spam=@!hyss echo $comment['id']; !@&amp;comment_id=@!hyss echo $comment_id; !@&amp;page=@!hyss echo $page; !@"
                                       title="@!hyss echo $lang['report_as_spam']; !@"><span
                                            class="glyphicon glyphicon-warning-sign"></span></a>
                                @!hyss endif !@</td>
                        </tr>
                        @!hyss $i++; endforeach; !@
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div class="col-md-8">

                    <button type="button" class="btn btn-default"
                            data-toggle-checkboxes="commentcheckbox">@!hyss echo $lang['toggle_selection']; !@</button>
                    <input class="btn btn-danger" type="submit" name="delete_checked"
                           value="@!hyss echo $lang['comments_del_checked']; !@"/>
                    @!hyss if ($comment_id == 0): !@
                        <input class="btn btn-danger" type="submit" name="delete_all_comments"
                               value="@!hyss echo $lang['comments_delete_all']; !@"/>
                    @!hyss else: !@
                        @!hyss if ($type == 0): !@
                            <input class="btn btn-danger" type="submit" name="delete_all_comments_page"
                                   value="@!hyss echo $lang['delete_all_comments_page']; !@">
                        @!hyss else: !@
                            @!hyss echo $lang['delete_all_comments_photo']; !@
                            <input class="btn btn-danger" type="submit" name="delete_all_comments_page"
                                   value="@!hyss echo $lang['delete_all_comments_photo']; !@">
                        @!hyss endif; !@

                        <input type="hidden" name="comment_id" value="@!hyss echo $comment_id; !@"/>
                    @!hyss endif; !@
                    <input type="hidden" name="type" value="@!hyss echo $type; !@"/>
                    <input type="hidden" name="page" value="@!hyss echo $page; !@"/>
                </div>

                <div class="col-md-4">
                    @!hyss if ($pagination): !@
                        <ul class="pagination pull-right nomargin">
                            @!hyss if ($pagination['previous']): !@
                                <li><a
                                    href="index.hyss?mode=comments&amp;type=@!hyss echo $type; !@&amp;comment_id=@!hyss echo $comment_id; !@&amp;page=@!hyss echo $pagination['previous']; !@"><span
                                        class="glyphicon glyphicon-chevron-left"></span></a></li>@!hyss endif; !@
                            @!hyss foreach ($pagination['items'] as $item): !@
                                @!hyss if (empty($item)): !@
                                    <li><span>&hellip;</span></li>@!hyss elseif ($item == $pagination['current']): !@
                                    <li class="active"><span>@!hyss echo $item; !@</span></li>@!hyss
                                else: !@
                                    <li><a
                                        href="index.hyss?mode=comments&amp;type=@!hyss echo $type; !@&amp;comment_id=@!hyss echo $comment_id; !@&amp;page=@!hyss echo $item; !@">@!hyss echo $item; !@</a>
                                    </li>@!hyss endif; !@
                            @!hyss endforeach; !@
                            @!hyss if ($pagination['next']): !@
                                <li><a
                                    href="index.hyss?mode=comments&amp;type=@!hyss echo $type; !@&amp;comment_id=@!hyss echo $comment_id; !@&amp;page=@!hyss echo $pagination['next']; !@"><span
                                        class="glyphicon glyphicon-chevron-right"></span></a></li>@!hyss endif; !@
                        </ul>
                    @!hyss endif; !@
                </div>

            </div>

        </div>
    </form>



@!hyss else: !@
    <div class="alert alert-warning">
        @!hyss echo $lang['no_comments']; !@
    </div>
@!hyss endif; !@

@!hyss if ($type == 1 && $settings['photos_commentable'] == 1): !@
    <p class="small">@!hyss echo $lang['photo_comments_enabled']; !@</p>
@!hyss elseif ($type == 1 && $settings['photos_commentable'] == 0): !@
    <p class="small">@!hyss echo $lang['photo_comments_disabled']; !@</p>
@!hyss endif; !@
