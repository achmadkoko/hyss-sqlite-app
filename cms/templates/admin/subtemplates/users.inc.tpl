<div class="row">
    <div class="col-md-10">
        <h1>@!hyss echo $lang['users']; !@</h1>
    </div>
    <div class="col-md-2">
        @!hyss if ($user_type == 1): !@
            <a class="btn btn-success btn-top pull-right" href="index.hyss?mode=users&amp;action=new"><span
                    class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['create_user_account']; !@</a>
        @!hyss endif; !@
    </div>
</div>

@!hyss include(BASE_PATH.'cms/templates/admin/subtemplates/errors.inc.tpl'); !@

@!hyss if (isset($users)): !@

    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>@!hyss echo $lang['user_name']; !@</th>
                <th>@!hyss echo $lang['user_type']; !@</th>
                <th>@!hyss echo $lang['last_login']; !@</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @!hyss $i = 0;
            foreach ($users as $user): !@
                <tr>
                    <td>@!hyss echo htmlspecialchars(stripslashes($user['name'])); !@</td>
                    <td>@!hyss if ($user['type'] == 0) echo $lang['type_0']; elseif ($user['type'] == 1) echo $lang['type_1'] !@</td>
                    <td>@!hyss if ($user['last_login']) echo strftime($lang['time_format'], $user['last_login']); !@</td>
                    <td class="options">@!hyss if ($user_type == 1 || $user_id == $user['id']): !@<a
                            class="btn btn-primary btn-xs"
                            href="index.hyss?mode=users&amp;edit=@!hyss echo $user['id']; !@"
                            title="@!hyss echo $lang['edit']; !@"><span class="glyphicon glyphicon-pencil"></span>
                            </a>@!hyss endif; !@
                        @!hyss if ($user_type == 1): !@<a class="btn btn-danger btn-xs"
                                                         href="index.hyss?mode=users&amp;delete=@!hyss echo $user['id']; !@"
                                                         title="@!hyss echo $lang['delete']; !@"
                                                         data-delete-confirm="@!hyss echo rawurlencode($lang['delete_user_confirm']); !@">
                                <span class="glyphicon glyphicon-remove"></span></a>@!hyss endif; !@</td>
                </tr>
                @!hyss $i++; endforeach; !@
            </tbody>
        </table>
    </div>

@!hyss else: !@

    <p>@!hyss echo $lang['no_users']; !@</p>

@!hyss endif; !@
