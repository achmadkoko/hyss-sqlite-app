<h1>@!hyss echo $lang['administration']; !@</h1>

@!hyss if (isset($msg)): !@
    <p class="ok">@!hyss if (isset($lang[$msg])) echo $lang[$msg]; else echo $msg; !@</p>
@!hyss endif; !@

<ul class="list-unstyled">
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=pages"><span
                class="glyphicon glyphicon-file"></span> @!hyss echo $lang['admin_menu_page_overview']; !@</a></li>
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=edit"><span
                class="glyphicon glyphicon-plus-sign"></span> @!hyss echo $lang['admin_menu_new_page']; !@</a></li>
</ul>
<ul class="list-unstyled">
    @!hyss if ($user_type == 1): !@
        <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=settings"><span
                class="glyphicon glyphicon-wrench"></span> @!hyss echo $lang['admin_menu_settings']; !@</a>
        </li>@!hyss endif; !@
    @!hyss if ($user_type == 1): !@
        <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=menus"><span
                class="glyphicon glyphicon-list-alt"></span> @!hyss echo $lang['admin_menu_edit_menus']; !@</a>
        </li>@!hyss endif; !@
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=galleries"><span
                class="glyphicon glyphicon-picture"></span> @!hyss echo $lang['admin_menu_edit_galleries']; !@</a></li>
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=comments"><span
                class="glyphicon glyphicon-comment"></span> @!hyss echo $lang['admin_menu_edit_comments']; !@</a></li>
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=notes"><span
                class="glyphicon glyphicon-edit"></span> @!hyss echo $lang['admin_menu_edit_notes']; !@</a></li>
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=gcb"><span
                class="glyphicon glyphicon-th-large"></span> @!hyss echo $lang['admin_menu_edit_gcb']; !@</a></li>
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=filemanager"><span
                class="glyphicon glyphicon-folder-open"></span> @!hyss echo $lang['admin_menu_filemanager']; !@</a></li>
    @!hyss if ($user_type == 1): !@
        <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=spam_protection"><span
                class="glyphicon glyphicon-ban-circle"></span> @!hyss echo $lang['admin_menu_spam_protection']; !@</a>
        </li>@!hyss endif; !@
    <li><a href="@!hyss echo BASE_URL; !@cms/index.hyss?mode=users"><span
                class="glyphicon glyphicon-user"></span> @!hyss if ($user_type == 1) echo $lang['admin_menu_user_administr']; else echo $lang['admin_menu_edit_userdata']; !@
        </a></li>
</ul>

@!hyss if ($settings['caching']): !@
    <ul class="list-unstyled">
        <li><a href="index.hyss?clear_cache=true"><span
                    class="glyphicon glyphicon-remove"></span> @!hyss echo $lang['admin_menu_clear_cache']; !@</a></li>
    </ul>
@!hyss endif; !@


