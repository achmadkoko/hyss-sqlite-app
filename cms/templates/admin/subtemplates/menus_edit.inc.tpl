<ol class="breadcrumb">
    <li><a href="index.hyss?mode=menus">@!hyss echo $lang['menus']; !@</a></li>
    <li class="active">@!hyss echo str_replace('[menu]', $menu, $lang['edit_menu_hl']); !@</li>
</ol>

<h1>@!hyss echo str_replace('[menu]', $menu, $lang['edit_menu_hl']); !@</h1>

<form action="index.hyss" method="post">
    <div>
        <input type="hidden" name="mode" value="menus"/>
        <input type="hidden" name="menu" value="@!hyss echo $menu; !@"/>
        @!hyss if (isset($edit_item)): !@
            <input type="hidden" name="edit_item" value="@!hyss echo $edit_item; !@"/>
        @!hyss else: !@
            <input type="hidden" name="new_menu_item" value="true"/>
        @!hyss endif; !@
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>@!hyss echo $lang['menu_item_name']; !@</th>
                    <th>@!hyss echo $lang['menu_item_title']; !@</th>
                    <th>@!hyss echo $lang['menu_item_link']; !@</th>
                    <th>@!hyss echo $lang['menu_item_section']; !@</th>
                    <th>@!hyss echo $lang['menu_item_accesskey']; !@</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody@!hyss if (empty($edit_item)): !@ data-sortable="@!hyss echo BASE_URL; !@cms/?mode=menus&amp;reorder_items=true"@!hyss endif; !@>

                @!hyss if (isset($edit_item)): !@
                    @!hyss $i = 0;
                    if (isset($items)): foreach ($items as $item): !@
                        <tr id="row_@!hyss echo $item['id']; !@"
                            class="@!hyss if ($i % 2 == 0): !@a@!hyss else: !@b@!hyss endif; !@">
                            <td>@!hyss if ($item['id'] == $edit_item): !@
                                    <input class="form-control" type="text"
                                           name="name"
                                           value="@!hyss echo $item['name']; !@"
                                           size="10"
                                           style="width:100%;" />@!hyss else: !@
                                    <span class="label label-default label-block">@!hyss echo $item['name']; !@</span>@!hyss endif; !@
                            </td>
                            <td>@!hyss if ($item['id'] == $edit_item): !@
                                    <input class="form-control" type="text"
                                           name="title"
                                           value="@!hyss echo $item['title']; !@"
                                           size="10"
                                           style="width:100%;" />@!hyss else: !@@!hyss echo $item['title']; !@@!hyss endif; !@
                            </td>
                            <td>@!hyss if ($item['id'] == $edit_item): !@
                                    <input class="form-control" type="text"
                                           name="link"
                                           value="@!hyss echo $item['link']; !@"
                                           size="10"
                                           style="width:100%;" />@!hyss else: !@@!hyss echo $item['link']; !@@!hyss endif; !@
                            </td>
                            <td>@!hyss if ($item['id'] == $edit_item): !@
                                    <input class="form-control" type="text"
                                           name="section"
                                           value="@!hyss echo $item['section']; !@"
                                           size="10"
                                           style="width:100%;" />@!hyss else: !@@!hyss echo $item['section']; !@@!hyss endif; !@
                            </td>
                            <td>@!hyss if ($item['id'] == $edit_item): !@
                                    <input class="form-control" type="text"
                                           name="accesskey"
                                           value="@!hyss echo $item['accesskey']; !@"
                                           size="10"
                                           style="width:100%;" />@!hyss else: !@@!hyss echo $item['accesskey']; !@@!hyss endif; !@
                            </td>
                            <td>@!hyss if ($item['id'] == $edit_item): !@
                                    <input class="btn btn-primary" type="submit"
                                           name="edit_menu_item_submitted"
                                           value="@!hyss echo $lang['submit_button_ok']; !@" />@!hyss else: !@&nbsp;@!hyss endif; !@
                            </td>
                        </tr>
                        @!hyss ++$i; endforeach; endif; !@

                @!hyss else: !@

                @!hyss $i = 0;
                if (isset($items)): foreach ($items as $item): !@
                    <tr id="item_@!hyss echo $item['id']; !@">
                        <td><span class="label label-default label-block">@!hyss echo $item['name']; !@</span></td>
                        <td>@!hyss echo $item['title']; !@</td>
                        <td>@!hyss echo $item['link']; !@</td>
                        <td>@!hyss echo $item['section']; !@</td>
                        <td>@!hyss echo $item['accesskey']; !@</td>
                        <td class="options nowrap">
                            <a class="btn btn-primary btn-xs"
                               href="index.hyss?mode=menus&amp;action=edit_menu_item&amp;id=@!hyss echo $item['id']; !@"
                               title="@!hyss echo $lang['edit']; !@">
                               <span class="glyphicon glyphicon-pencil"></span></a>
                            <a class="btn btn-danger btn-xs"
                               href="index.hyss?mode=menus&amp;action=delete_menu_item&amp;id=@!hyss echo $item['id']; !@"
                               title="@!hyss echo $lang['delete']; !@">
                               <span class="glyphicon glyphicon-remove"></span></a>
                            <span class="btn btn-success btn-xs sortable-handle"
                                  title="@!hyss echo $lang['drag_and_drop']; !@">
                                <span class="glyphicon glyphicon-sort"></span></span>
                            <!--<a href="index.hyss?mode=menus&amp;move_up=@!hyss echo $item['id']; !@" title="@!hyss echo $lang['move_up']; !@">@!hyss echo $lang['move_up']; !@</a><a href="index.hyss?mode=menus&amp;move_down=@!hyss echo $item['id']; !@" title="@!hyss echo $lang['move_down']; !@">@!hyss echo $lang['move_down']; !@</a>-->
                        </td>
                    </tr>
                    @!hyss ++$i; endforeach; endif; !@
                </tbody>
                <tr>
                    <td><input class="form-control" type="text" name="name" value="" size="10"/></td>
                    <td><input class="form-control" type="text" name="title" value="" size="10"/></td>
                    <td><input class="form-control" type="text" name="link" value="" size="10"/></td>
                    <td><input class="form-control" type="text" name="section" value="" size="10"/></td>
                    <td><input class="form-control" type="text" name="accesskey" value="" size="3"/></td>
                    <td class="options">
                        <button class="btn btn-success"><span
                                class="glyphicon glyphicon-plus"></span> @!hyss echo $lang['add_menu_item_submit']; !@
                        </button>
                    </td>
                </tr>

                @!hyss endif; !@


            </table>
        </div>
    </div>
</form>
