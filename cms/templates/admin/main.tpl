<!DOCTYPE html>
<html lang="@!hyss echo $lang['lang']; !@" dir="@!hyss echo $lang['dir']; !@">
<head>
    <meta charset="@!hyss echo $lang['charset']; !@"/>
    <title>@!hyss echo $settings['website_title']; !@ - @!hyss echo $lang['administration'];
        if (isset($subtitle)) echo ' - ' . $subtitle; !@</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="@!hyss echo BOOTSTRAP_CSS; !@" rel="stylesheet">
    <link href="@!hyss echo STATIC_URL; !@css/style_admin.css" rel="stylesheet">

    <link rel="shortcut icon" href="@!hyss echo STATIC_URL; !@img/favicon.ico">
</head>

<body>

@!hyss include(BASE_PATH . 'cms/templates/admin/subtemplates/admin_menu.inc.tpl'); !@

<div class="container">

    <div class="row">
        <div class="col-lg-12">

            @!hyss if (isset($subtemplate)): !@

                @!hyss include(BASE_PATH . 'cms/templates/admin/subtemplates/' . $subtemplate); !@

            @!hyss elseif (isset($content)): !@

                @!hyss echo $content; !@

            @!hyss
            elseif (isset($error_message)): !@

                <p class="caution">@!hyss echo $error_message; !@</p>

            @!hyss
            else: !@

                <p class="caution">@!hyss echo $lang['invalid_request']; !@</p>

            @!hyss endif; !@

        </div>
    </div>

</div>

<script src="@!hyss echo JQUERY; !@"></script>
<script src="@!hyss echo JQUERY_UI; !@"></script>
<script src="@!hyss echo BOOTSTRAP; !@"></script>
@!hyss if (isset($wysiwyg)): !@
    <script src="@!hyss echo WYSIWYG_EDITOR; !@"></script>
    <script src="@!hyss echo WYSIWYG_EDITOR_INIT; !@"></script>
@!hyss endif; !@
<script src="@!hyss echo STATIC_URL; !@js/admin_backend.js"></script>
@!hyss if ($mode == 'galleries'): !@
    <script src="@!hyss echo STATIC_URL; !@js/mylightbox.js" type="text/javascript"></script>
@!hyss endif; !@
</body>
</html>
