@!hyss echo '<?xml version="1.0" encoding="' . $lang['charset'] . '" ?>'; !@
<urlset xmlns="http://www.google.com/schemas/sitemap/0.84" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">
    @!hyss if (isset($sitemap_items)): foreach ($sitemap_items as $item): !@
        <url>
            <loc>@!hyss echo $item['loc'] !@</loc>
            <lastmod>@!hyss echo $item['lastmod'] !@</lastmod>
        </url>
    @!hyss endforeach; endif; !@
</urlset>
