@!hyss echo '<?xml version="1.0" encoding="' . $lang['charset'] . '" ?>'; !@
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <title>@!hyss echo $title; !@</title>
        <link>@!hyss echo BASE_URL; !@</link>
        <description>@!hyss echo $description; !@</description>
        <language>@!hyss echo $lang['lang']; !@</language>
        @!hyss if (isset($rss_items)): foreach ($rss_items as $item): !@
            <item>
                <title>@!hyss echo $item['title'] !@</title>
                <content:encoded><![CDATA[@!hyss echo $item['content'] !@]]></content:encoded>
                <link>@!hyss echo $item['link'] !@</link>
                <pubDate>@!hyss echo $item['pubdate'] !@</pubDate>
            </item>
        @!hyss endforeach; endif; !@
    </channel>
</rss>
