hyssSQLiteApp
============

[hyssSQLiteApp](http://bitbucket.org/achmadkoko/hyss-sqlite-app/) is a simple and lightweight open source web content management system (CMS) based on [HySS](https://bitbucket.org/hyw3/hyss-dev/) and [SQLite](https://www.sqlite.org/). As SQLite is file-based, it just runs "out of the box" without installation.

System requirements
-------------------

* **cLHy 1.6.26+** with **capi_rewrite** and **.htaccess** file support enabled
* **HySS 1.0.13+** with PDO and SQLite driver enabled

Installation
------------

1. Load up the script files to your server
2. Depending on your server configuration you may need to change the write permissions of the following files/directories:
     * **cms/data** - directory of the SQLite database files, needs to be writable by the webserver
     * **content.sqlite**, **entries.sqlite** and **userdata.sqlite** - SQLite database files, need to be writable by the webserver
     * **cms/cache** - cache directory, needs to be writable if you want to use the caching feature
     * **cms/media** and **cms/files** - need to be writable if you want to use the file uploader
3. Ready! You should now be able to access the index page by browsing to the address you uploaded hyssSQLiteApp (e.g. http://example.org/path/to/hyss-sqlite-app/). To administrate the page, go to http://example.org/path/to/hyss-sqlite-app/cms/. The default admin userdata is: **username: admin**, **password: admin**.
